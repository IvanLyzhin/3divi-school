import numpy as np

def save_pgm(filename, image):
	pgmf = open(filename, mode='wb')
	pgmf.write('P5\n'.encode())
	pgmf.write('{} {}\n'.format(image.shape[1], image.shape[0]).encode())
	pgmf.write('255\n'.encode())
	pgmf.write(bytearray([int(x) for x in image.flatten()]))
	pgmf.close()
	
	
def read_pgm(filename):
	pgmf = open(filename, 'rb')
	
	bts = pgmf.read()
	n1 = bts.index('\n'.encode())
	n2 = bts[n1+1:].index('\n'.encode()) + n1 + 1
	n3 = bts[n2+1:].index('\n'.encode()) + n2 + 1
	header = bts[:n3+1]
	pixels = bts[n3+1:]
	
	a = header.index('\n'.encode())
	b = header.index(' '.encode())
	c = header[a+1:].index('\n'.encode())+a+1
	width = int(header[a+1:b].decode())
	height = int(header[b+1:c].decode())
	print((height, width))
	
	image = np.zeros((height, width))
	for x in range(height):
		for y in range(width):
			image[x][y] = pixels[x*width+y]
	pgmf.close()
	return image