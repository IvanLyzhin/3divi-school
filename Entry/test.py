from math import sqrt, hypot, sin, cos, pi
import pgm_utils
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

class Point:
	def __init__(self, x, y):
		self.x = x
		self.y = y
		
	def __str__(self):
		return '({}, {})'.format(self.x, self.y)
		
	def equal(self, other):
		return self.x == other.x and self.y == other.y
		
	def equal_eps(self, other, eps):
		return abs(self.x-other.x)<eps and abs(self.y-other.y)<eps
		
	def add(self, other):
		return Point(self.x + other.x, self.y + other.y)
		
	def sub(self, other):
		return Point(self.x - other.x, self.y - other.y)
		
	def is_valid(self, image_shape):
		return self.x>=0 and self.y>=0 and self.x<image_shape[0] and self.y<image_shape[1]


class Line:
	def __init__(self, A, B, C):
		self.A = A
		self.B = B
		self.C = C
		
	def intersectWith(self, line):
		if abs(self.A*line.B-self.B*line.A)<1e-5:
			return None
		x = (self.C*line.B - self.B*line.C)/(self.A*line.B - self.B*line.A)
		y = -(self.C*line.A - self.A*line.C)/(self.A*line.B - self.B*line.A)
		return Point(int(x), int(y))

dxs = []
dys = []
zs = []
		
def hough(image):
	"Calculate Hough transform."
	height, width = image.shape
	rmax = int(hypot(height, width)+1)
	him = np.zeros((2*rmax, 180))
	cnt = np.zeros((2*rmax, 180))
	print(him.shape)

	for x in range(height):
		for y in range(width):
			col = image[x][y]
			for th in range(180):
				th_rad = th*pi/180
				r = int(x*cos(th_rad) + y*sin(th_rad))
				hx = rmax + r
				cnt[hx][th] += 1
	for i in range(him.shape[0]):
		for j in range(him.shape[1]):
			r = i - rmax;
			th_rad = th*pi/180
			line = Line(cos(th_rad), sin(th_rad), r);
			xs = [0]*4
			ys = [0]*4
			xs[0] = line.intersectWith(Line(1, 0, 0)).x;
			xs[1] = line.intersectWith(Line(1, 0, 500)).x;
			xs[2] = line.intersectWith(Line(0, 1, 0)).x;
			xs[3] = line.intersectWith(Line(0, 1, 500)).x;
			ys[0] = line.intersectWith(Line(1, 0, 0)).y;
			ys[1] = line.intersectWith(Line(1, 0, 500)).y;
			ys[2] = line.intersectWith(Line(0, 1, 0)).y;
			ys[3] = line.intersectWith(Line(0, 1, 500)).y;
			xs.sort()
			ys.sort()
			dxs.append(xs[2]-xs[1])
			dys.append(ys[2]-ys[1])
			zs.append(cnt[i][j])
			
image = pgm_utils.read_pgm('image.pgm')
hough(image)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
plt.scatter(dxs, dys, zs)
plt.show()
			