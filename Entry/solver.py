﻿import sys
import generator
import restorator

def generate(P):
	print('Generating with P=%.1f' % P)	
	generator.generate(P)
	
	
def restore(filename):
	print('Restoring from file %s' % filename)
	restorator.restore(filename)

	
def help():
	print('Usage: solver.py -generate P | solver.py -restore image.pgm')
	
def main():
	if len(sys.argv)<2:
		help()
	elif sys.argv[1]=='-generate':
		generate(float(sys.argv[2]))
	elif sys.argv[1]=='-restore':
		restore(sys.argv[2])
	else:
		help()
        	
	
if __name__=='__main__':
	main()