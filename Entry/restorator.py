import pylab
import numpy as np
import pgm_utils
from math import hypot, pi, cos, sin, sqrt, fabs, floor
import generator
from point import Point
	
def draw(points, image):
	for p in points:
		image[p.x][p.y] = 255


def norm_to_gray(image):
	print(image)
	norm_image = np.zeros(image.shape, dtype=int)
	mx = np.max(image.flatten())
	print('Mx: %f' % mx)
	for i in range(image.shape[0]):
		for j in range(image.shape[1]):
			norm_image[i][j] = int(round(255*image[i][j]/mx))
	return norm_image
	
	
def hough(image):
	"Calculate Hough transform."
	height, width = image.shape
	rmax = int(hypot(height, width)+1)
	him = np.zeros((2*rmax, 180))
	cnt = np.zeros((2*rmax, 180))
	print(him.shape)

	for x in range(height):
		for y in range(width):
			col = image[x][y]
			for th in range(180):
				th_rad = th*pi/180
				r = int(x*cos(th_rad) + y*sin(th_rad))
				hx = rmax + r
				him[hx][th] += col
				cnt[hx][th] += 1
	for i in range(him.shape[0]):
		for j in range(him.shape[1]):
			if cnt[i][j]<200:
				him[i][j]=0
			else:
				him[i][j] = him[i][j]/cnt[i][j]
	return norm_to_gray(him)
	
	
class Line:
	def __init__(self, A, B, C):
		self.A = A
		self.B = B
		self.C = C
		
	def intersect_with(self, line):
		if abs(self.A*line.B-self.B*line.A)<1e-5:
			return None
		x = (self.C*line.B - self.B*line.C)/(self.A*line.B - self.B*line.A)
		y = -(self.C*line.A - self.A*line.C)/(self.A*line.B - self.B*line.A)
		return Point(int(x), int(y))


def valid(image, p):
	return p.x>=0 and p.y>=0 and p.x<image.shape[0] and p.y<image.shape[1]
		
		
def power(image, p1, p2):
	if not valid(image, p1) or not valid(image, p2):
		return int(-1e9)
	ret = 0
	if abs(p1.x - p2.x) > abs(p1.y - p2.y):
		if p1.x > p2.x:
			p1, p2 = p2, p1
		dy = (p2.y - p1.y)/(p2.x - p1.x)
		for x in range(p1.x, p2.x+1):
			y = int(round(p1.y + (x-p1.x)*dy))
			ret += image[x][y]
	else:
		if p1.y > p2.y:
			p1, p2 = p2, p1
		dx = (p2.x - p1.x)/(p2.y - p1.y)
		for y in range(p1.y, p2.y+1):
			x = int(round(p1.x + (y-p1.y)*dx))
			ret += image[x][y]
	return ret/distXY(p1, p2)
	
	
def draw_bres(image, p1, p2):
	if not valid(image, p1) or not valid(image, p2): return
	if abs(p1.x - p2.x) > abs(p1.y - p2.y):
		if p1.x > p2.x:
			p1, p2 = p2, p1
		dy = (p2.y - p1.y)/(p2.x - p1.x)
		for x in range(p1.x, p2.x+1):
			y = int(round(p1.y + (x-p1.x)*dy))
			image[x][y] = 255
	else:
		if p1.y > p2.y:
			p1, p2 = p2, p1
		dx = (p2.x - p1.x)/(p2.y - p1.y)
		for y in range(p1.y, p2.y+1):
			x = int(round(p1.x + (y-p1.y)*dx))
			image[x][y] = 255
	
def distXY(pA, pB):
    """Расстояние между проекциями точек в плоскости OXY"""
    return sqrt((pA.x - pB.x)**2 + (pA.y - pB.y)**2)


def distToLine(p, pA, pB):
    """Расстояние от точки до прямой, заданной двумя точками"""
    u = pA.sub(p)
    v = pB.sub(p)
    s = fabs(u.x*v.y-u.x*v.y)
    return s/distXY(pA, pB)


def validate_cube(vertices):
    """Проверка куба на нарушений ограничений"""
    # Проверка на выход за границы изображения
    for point in vertices:
        if point.x<0 or point.x>=500 or point.y<0 or point.y>=500:
            return False
        
    # Проверка на близость вершин
    for pA in vertices:
        for pB in vertices:
            if not pA.equal(pB) and distXY(pA, pB)<100:
                return False
    
    # Проверка на близость параллельных ребер
    parallels = [[[0, 1], [2, 3], [4, 5], [6, 7]], [[0, 3], [1, 2], [4, 7], [5, 6]], [[0, 4], [1, 5], [2, 6], [3, 7]]]
    for group in parallels:
        for l1 in group:
            for l2 in group:
                if not np.array_equal(l1, l2):
                    if distToLine(vertices[l1[0]], vertices[l2[0]], vertices[l2[1]])<50:
                        return False
    return True	

	
def restore(filename):
	image = pgm_utils.read_pgm(filename)
	#image = threshold_filter(image, 165)
	#pgm_utils.save_pgm('threshold.pgm', image)
	
	#image = median_filter(image)
	#pgm_utils.save_pgm('median.pgm', image)
	
	#contours = find_contours(image)
	#print(len(contours))
	#cont_image = np.zeros_like(image)
	#for c in contours:
	#	print(len(c))
	#	draw(c, cont_image)
	#pgm_utils.save_pgm('contours.pgm', cont_image)
	hough_image = hough(image)
	pgm_utils.save_pgm('hough.pgm', hough_image)
	
	values = []
	for i in range(hough_image.shape[0]):
		for j in range(0, hough_image.shape[1]):
			if hough_image[i][j]>0:
				r = i - hough_image.shape[0]//2
				values.append((r, j, hough_image[i][j]))
	angle_values = []
	for j in range(hough_image.shape[1]):
		a = []
		for i in range(hough_image.shape[0]):
			r = i - hough_image.shape[0]//2
			a.append((hough_image[i][j], r))
		a.sort()
		a.reverse()
		b = []
		s = 0
		for val, r in a:
			take = True
			for r2 in b:
				if abs(r-r2)<45:
					take = False
			if take:
				b.append(r)
				s += val
			if len(b)==4: break
		angle_values.append((s, b, j))
	angle_values.sort()
	angle_values.reverse()
	
	taken = []
	for _, rs, th in angle_values:
		take = True
		for th2, _ in taken:
			if abs(th-th2)<30:
				take = False
		if take:
			taken.append((th, rs))
		if len(taken)==2:
			break
	
	temp_image = np.zeros_like(image)	
	edges = []
	for th, rs in taken:
		th_rad = th * pi / 180
		rs.sort()
		for r in rs:
			edges = np.append(edges, Line(cos(th_rad), sin(th_rad), r))
			print(r, th)
			if th>45 and th<135:
				for x in range(temp_image.shape[0]):
					y = int(round((r-x*cos(th_rad))/sin(th_rad)))
					if y>=0 and y<temp_image.shape[1]:
						temp_image[x][y] = 255
			else:
				for y in range(temp_image.shape[1]):
					x = int(round((r-y*sin(th_rad))/cos(th_rad)))
					if x>=0 and x<temp_image.shape[0]:
						temp_image[x][y] = 255
	pgm_utils.save_pgm('temp.pgm', temp_image)
	
	variants = [
		[(0, 0), (0, 2), (1, 2), (1, 0), (2, 1), (2, 3), (3, 3), (3, 1)],
		[(0, 1), (0, 3), (1, 3), (1, 1), (2, 0), (2, 2), (3, 2), (3, 0)],
		[(0, 0), (0, 2), (2, 2), (2, 0), (1, 1), (1, 3), (3, 3), (3, 1)],
		[(0, 1), (0, 3), (2, 3), (2, 1), (1, 0), (1, 2), (3, 2), (3, 0)],
		[(0, 0), (0, 2), (3, 2), (3, 0), (1, 1), (1, 3), (2, 3), (2, 1)],
		[(0, 1), (0, 3), (3, 3), (3, 1), (1, 0), (1, 2), (2, 2), (2, 0)]
	]
	vertices = []
	for v in variants:
		vert = []
		for i, j in v:
			vert.append(edges[i].intersect_with(edges[j+4]))
		vertices.append(vert)
	powers = []
	for i in range(len(variants)):
		p = 0
		for j in range(4):
			p += power(image, vertices[i][j], vertices[i][j+4])
		if not validate_cube(vertices[i]):
			p -= int(1e9)
		powers.append(p)
	print('Powers: {}'.format(powers))
	
	result_vertices = vertices[np.argmax(powers)]
	print('Vertices: ')
	print('\n'.join([str(x) for x in result_vertices]))
	result_image = np.zeros_like(image)
	for i in range(4):
		draw_bres(result_image, result_vertices[i], result_vertices[i+4])
		draw_bres(result_image, result_vertices[i], result_vertices[(i+1)%4])
		draw_bres(result_image, result_vertices[i+4], result_vertices[(i+1)%4+4])
	pgm_utils.save_pgm('result.pgm', result_image)
	
	