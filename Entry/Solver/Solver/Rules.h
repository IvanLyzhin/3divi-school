#ifndef RULES_H
#define RULES_H
#include <vector>
#include "Point.h"
using namespace std;

class Rules
{
public:
	static const int minLength = 150;
	static const int maxLength = 300;

	static const int imageWidth = 500;
	static const int imageHeight = 500;

	static bool isPointValid(const Point &point);
	static bool isCubeValid(const vector<Point> &vertices, double eps = 0);
};

#endif
