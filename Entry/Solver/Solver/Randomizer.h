#ifndef RANDOMIZER_H
#define RANDOMIZER_H
#include <random>
using namespace std;

class Randomizer
{
	static default_random_engine engine;

public:
	static int getNextInt(int min, int max);
	static double getNextDouble(int min, int max);
};

#endif
