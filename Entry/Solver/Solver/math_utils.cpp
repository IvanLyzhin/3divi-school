#include <ctime>
#include <random>
#include "utils.h"
using namespace std;

v2dbl multiply(v2dbl a, v2dbl b)
{
	int n = a.size();
	int m = b.size();
	int p = b[0].size();
	v2dbl c(n, vdbl(p));
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < p; ++j)
			for (int k = 0; k < m; ++k)
				c[i][j] += a[i][k] * b[k][j];
	return c;
}

double ipart(double x)
{
	return floor(x);
}

double fpart(double x)
{
	return x - ipart(x);
}

double rfpart(double x)
{
	return 1 - fpart(x);
}

int myround(double x)
{
	return (int)(x + 0.5);
}