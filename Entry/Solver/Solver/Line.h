#ifndef LINE_H
#define LINE_H
#include "Point.h"

struct Line
{
	double a;
	double b;
	double c;

	Line(double a, double b, double c) : a(a), b(b), c(c) {}

	Point intersectWith(const Line other) const
	{
		double d = a*other.b - b*other.a;
		if (abs(d) < 1e-6) return Point(-1e9, -1e9);
		double dx = c*other.b - b*other.c;
		double dy = a*other.c - c*other.a;
		return Point(dx / d, dy / d);
	}
};

#endif
