#ifndef WU_LINER_H
#define WU_LINER_H

struct Line;

class WuLiner
{
public:
	static void drawLine(PGM_Image &image, Point pA, Point pB);
	static void drawLine(PGM_Image &image, Line line);
	static vector<Point> getPoints(Point pA, Point pB);
	static void drawCube(PGM_Image &image, const vector<Point> &vertices);
};

#endif
