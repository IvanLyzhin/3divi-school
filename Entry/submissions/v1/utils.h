#ifndef UTILS_H
#define UTILS_H

#include <vector>
using namespace std;

typedef vector<int> vint;
typedef vector<vint> v2int;

typedef vector<char> vchr;
typedef vector<vchr> v2chr;

typedef vector<double> vdbl;
typedef vector<vdbl> v2dbl;

typedef pair<int, int> pii;

typedef unsigned char byte;
typedef vector<byte> vbyte;
typedef vector<vbyte> v2byte;

template <typename T>
int argmax(vector<T> &v)
{
	T best = v[0];
	int bestIndex = 0;
	for(int i=1; i<v.size(); ++i)
		if(v[i]>best)
		{
			best = v[i];
			bestIndex = i;
		}
	return bestIndex;
}

#endif
