#ifndef CUBE_GENERATOR_H
#define CUBE_GENERATOR_H
#include "PGM_Image.h"
#include "Point.h"

class CubeGenerator
{
public:
	vector<Point> generateCube() const;
};

#endif