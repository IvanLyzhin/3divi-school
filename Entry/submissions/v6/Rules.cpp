#include "Rules.h"

bool Rules::isPointValid(const Point& point)
{
	return point.x >= 0 && point.y >= 0 && point.x < imageWidth && point.y < imageHeight;
}

bool Rules::isCubeValid(const vector<Point>& vertices, double eps)
{
	// �������� �� ����� �� ������� �����������
	for (auto v : vertices)
		if (!isPointValid(v))
			return false;

	// �������� �� �������� ������
	for (int i = 0; i < 8; ++i)
		for (int j = i + 1; j < 8; ++j)
			if (vertices[i].distTo(vertices[j]) < 100 - eps)
				return false;

	// �������� �� �������� ������������ �����
	vector<vector<pii>> parallels = {
		{ pii(0, 1), pii(2, 3), pii(4, 5), pii(6, 7) },
		{ pii(0, 3), pii(1, 2), pii(4, 7), pii(5, 6) },
		{ pii(0, 4), pii(1, 5), pii(2, 6), pii(3, 7) },
	};
	for (auto group : parallels)
		for (int i = 0; i < 4; ++i)
			for (int j = i + 1; j < 4; ++j)
				if (vertices[group[i].first].distTo(vertices[group[j].first], vertices[group[j].second]) < 50 - eps)
					return false;

	return true;
}
