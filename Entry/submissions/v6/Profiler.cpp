#include "Profiler.h"
#include <ctime>
#include <cstdio>

double Profiler::prevCheckPointTime = 0;

void Profiler::checkpoint(char *label)
{
	double curTime = (double)clock() / CLOCKS_PER_SEC;
	printf("%s. Current time: %.2f sec; From previous checkpoint: %.2f sec\n", label, curTime, curTime - prevCheckPointTime);
	prevCheckPointTime = curTime;
}