#include "CubeGenerator.h"
#include "Rules.h"
#include "math_utils.h"
#include "Randomizer.h"

vector<Point> CubeGenerator::generateCube() const
{
	while (true)
	{
		// ����� ������� ����
		double length = Randomizer::getNextDouble(Rules::minLength, Rules::maxLength);
		// ���� ��������
		double alpha = Randomizer::getNextDouble(0, 2 * PI);
		double beta = Randomizer::getNextDouble(0, 2 * PI);
		double gamma = Randomizer::getNextDouble(0, 2 * PI);
		// ������� ��������
		v2dbl rx = { { 1, 0, 0 },{ 0, cos(alpha), -sin(alpha) },{ 0, sin(alpha), cos(alpha) } };
		v2dbl ry = { { cos(beta), 0, sin(beta) },{ 0, 1, 0 },{ -sin(beta), 0, cos(beta) } };
		v2dbl rz = { { cos(gamma), -sin(gamma), 0 },{ sin(gamma), cos(gamma), 0 },{ 0, 0, 1 } };
		v2dbl r = multiply(multiply(rx, ry), rz);
		// ����� ����
		Point center(Randomizer::getNextInt(0, 499), Randomizer::getNextInt(0, 499));
		// ���������� ������
		vector<Point> vertices;
		for (int i = -1; i <= 1; i += 2)
			for (int j = -1; j <= 1; j += 2)
				for (int k = -1; k <= 1; k += 2)
					vertices.push_back(Point(i*length / 2, j*length / 2, k*length / 2));
		// ����� ������� ��� �� ������� ������
		swap(vertices[2], vertices[3]);
		swap(vertices[6], vertices[7]);
		// ���������� ��� � �������� � ������ �����
		for (int i = 0; i < 8; ++i)
			vertices[i] = vertices[i].rotate(r) + center;

		if(Rules::isCubeValid(vertices))
			return vertices;
	}
}
