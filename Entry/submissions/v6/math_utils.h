#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include <cmath>
using namespace std;

const double PI = atan(1) * 4;

v2dbl multiply(v2dbl a, v2dbl b);
double ipart(double x);
double fpart(double x);
double rfpart(double x);
int myround(double x);


#endif