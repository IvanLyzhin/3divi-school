#ifndef CUBE_RESTORATOR_H
#define CUBE_RESTORATOR_H
#include "Point.h"
#include <vector>
using namespace std;

class CubeRestorator
{
public:
	vector<Point> restoreCube(PGM_Image image) const;

private:
	vint getTop2Angles(v2dbl hough) const;
	vint getTop4Ry(v2dbl hough, int th) const;
	double calcPower(PGM_Image &image, Point pA, Point pB) const;
};

#endif
