#include "Point.h"
#include "WuLiner.h"
#include "math_utils.h"
#include "Line.h"
#include "Rules.h"

void WuLiner::drawLine(PGM_Image& image, Line line)
{
	if (abs(line.a) > abs(line.b))
		drawLine(image, Point(line.c / line.a, 0), Point((line.c - (image.width - 1)*line.b) / line.a, image.width - 1));
	else
		drawLine(image, Point(0, line.c / line.b), Point(image.height - 1, (line.c - (image.height - 1)*line.a) / line.b));

}

void WuLiner::drawLine(PGM_Image &image, Point pA, Point pB)
{
	for (auto point : getPoints(pA, pB))
		if(Rules::isPointValid(point))
			image.pixels[point.y][point.x] = (byte)(point.z * 255);
}

vector<Point> WuLiner::getPoints(Point pA, Point pB)
{
	vector<Point> points;

	bool steep = abs(pA.y - pB.y) > abs(pA.x - pB.x);

	if (steep)
	{
		swap(pA.x, pA.y);
		swap(pB.x, pB.y);
	}

	if (pA.x > pB.x)
		swap(pA, pB);

	double dx = pB.x - pA.x;
	double dy = pB.y - pA.y;
	double gradient = dy / dx;
	if (dx == 0.0)
		gradient = 1.0;

	// handle first endpoint
	double xend = round(pA.x);
	double yend = pA.y + gradient * (xend - pA.x);
	double xgap = rfpart(pA.x + 0.5);
	int xpxl1 = xend;
	int ypxl1 = ipart(yend);
	if (steep)
	{
		points.push_back(Point(ypxl1, xpxl1, rfpart(yend) * xgap));
		points.push_back(Point(ypxl1 + 1, xpxl1, fpart(yend) * xgap));
	}
	else
	{
		points.push_back(Point(xpxl1, ypxl1, rfpart(yend) * xgap));
		points.push_back(Point(xpxl1, ypxl1 + 1, fpart(yend) * xgap));
	}
	double intery = yend + gradient;

	// handle second endpoint
	xend = round(pB.x);
	yend = pB.y + gradient * (xend - pB.x);
	xgap = fpart(pB.x + 0.5);
	int xpxl2 = xend;
	int ypxl2 = ipart(yend);
	if (steep)
	{
		points.push_back(Point(ypxl2, xpxl2, rfpart(yend)*xgap));
		points.push_back(Point(ypxl2 + 1, xpxl2, fpart(yend)*xgap));
	}
	else
	{
		points.push_back(Point(xpxl2, ypxl2, rfpart(yend) * xgap));
		points.push_back(Point(xpxl2, ypxl2 + 1, fpart(yend) * xgap));
	}

	// main loop
	if (steep)
	{
		for (int x = xpxl1 + 1; x<xpxl2; ++x)
		{
			points.push_back(Point((int)ipart(intery), x, rfpart(intery)));
			points.push_back(Point((int)ipart(intery) + 1, x, fpart(intery)));
			intery += gradient;
		}
	}
	else
	{
		for (int x = xpxl1 + 1; x<xpxl2; ++x)
		{
			points.push_back(Point(x, (int)ipart(intery), rfpart(intery)));
			points.push_back(Point(x, (int)ipart(intery) + 1, fpart(intery)));
			intery += gradient;
		}
	}
	return points;
}

void WuLiner::drawCube(PGM_Image& image, const vector<Point>& vertices)
{
	for (int i = 0; i<4; ++i)
	{
		drawLine(image, vertices[i], vertices[i + 4]);
		drawLine(image, vertices[i], vertices[(i + 1) % 4]);
		drawLine(image, vertices[i + 4], vertices[(i + 1) % 4 + 4]);
	}
}
