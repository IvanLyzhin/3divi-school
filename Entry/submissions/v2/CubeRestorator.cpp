#include "CubeRestorator.h"
#include "HoughTransformer.h"
#include <algorithm>
#include "Line.h"
#include "Rules.h"
#include "WuLiner.h"
#include "Profiler.h"

vector<Point> CubeRestorator::restoreCube(PGM_Image image) const
{
	// ������ �������������� �����
	printf("Hough tranformation...\n");
	Profiler::checkpoint("Start hough transformation");
	v2dbl hough = HoughTransformer().transform(image);
	Profiler::checkpoint("End hough transformation");
	// ������� 2 ����� ������ ����
	// �������� ���� - ��� ����� ��������� ��� 4 ������ �������� ����� ����
	printf("Finding best angles...\n");
	Profiler::checkpoint("Start finding best angles");
	vint top2angles = getTop2Angles(hough);
	Profiler::checkpoint("End finding best angles");
	printf("Best angles: %d %d\n", top2angles[0], top2angles[1]);
	// �������� 8 ����� - �� 4 ��� ������� ����
	vector<Line> lines;
	printf("Lines:\n");
	for (auto th : top2angles) {
		double th_rad = th*PI / 180;
		auto rys = getTop4Ry(hough, th);
		sort(rys.begin(), rys.end());
		for (auto ry : rys)
		{
			int r = ry - hough.size() / 2;
			lines.push_back(Line(cos(th_rad), sin(th_rad), r));
			printf("%.3fX + %.3fY = %.3f\n", lines.back().a, lines.back().b, lines.back().c);
		}
	}
	
	// ����������� ����� ����� ���������, �� ����������� 16, � ������ 8. ���� ��������� �������� ������
	vector<vector<pii>> variants;
	variants.push_back({ pii(0, 0), pii(0, 2), pii(1, 2), pii(1, 0), pii(2, 1), pii(2, 3), pii(3, 3), pii(3, 1) });
	variants.push_back({ pii(0, 1), pii(0, 3), pii(1, 3), pii(1, 1), pii(2, 0), pii(2, 2), pii(3, 2), pii(3, 0) });
	variants.push_back({ pii(0, 0), pii(0, 2), pii(2, 2), pii(2, 0), pii(1, 1), pii(1, 3), pii(3, 3), pii(3, 1) });
	variants.push_back({ pii(0, 1), pii(0, 3), pii(2, 3), pii(2, 1), pii(1, 0), pii(1, 2), pii(3, 2), pii(3, 0) });
	variants.push_back({ pii(0, 0), pii(1, 0), pii(3, 1), pii(2, 1), pii(0, 2), pii(1, 2), pii(3, 3), pii(2, 3) });
	variants.push_back({ pii(0, 1), pii(1, 1), pii(3, 0), pii(2, 0), pii(0, 3), pii(1, 3), pii(3, 2), pii(2, 2) });
	
	// ������� ��� ������� ��������
	vector<vector<Point> > var_vertices;
	for(auto v : variants)
	{
		vector<Point> vertices;
		for (auto p : v)
			vertices.push_back(lines[p.first].intersectWith(lines[p.second + 4]));
		var_vertices.push_back(vertices);
	}

	// ��� ������� �������� ��������� ��� ��������, ��� ����� �������� �������� �� ������
	printf("Calculating power for variants...\n");
	Profiler::checkpoint("Start power calculation");
	vector<double> powers;
	for(auto &vertices : var_vertices)
	{
		double power = 0;
		for (int i = 0; i < 4; ++i)
			power += calcPower(image, vertices[i], vertices[i + 4]);
		if (!Rules::isCubeValid(vertices, 5))
			power -= 1e9;
		vdbl lengths = { vertices[0].distTo(vertices[1]), vertices[0].distTo(vertices[3]), vertices[0].distTo(vertices[4]) };
		double disbalance = max(lengths) - min(lengths);
		power -= disbalance*10;
		powers.push_back(power);
	}
	Profiler::checkpoint("End power calculation");
	printf("Powers: ");
	for (auto p : powers)
		printf("%.3f ", p);
	printf("\n");
	
	// ������� ������� � ���������� ���������
	vector<Point> ans_vertices = var_vertices[argmax(powers)];
	return ans_vertices;
}

vint CubeRestorator::getTop4Ry(v2dbl hough, int th) const
{
	vector<pair<double, int> > rScores(hough.size());
	for (int ry = 0; ry < hough.size(); ++ry)
		rScores[ry] = pair<double, int>(hough[ry][th], ry);
	sort(rScores.begin(), rScores.end());
	reverse(rScores.begin(), rScores.end());
	vint takenRs;
	for (int i = 0; i<rScores.size() && takenRs.size()<4; ++i)
	{
		bool take = true;
		for (auto r : takenRs)
			if (abs(r - rScores[i].second) < 45)
				take = false;
		if (take)
			takenRs.push_back(rScores[i].second);
	}
	return takenRs;
}

double CubeRestorator::calcPower(PGM_Image& image, Point pA, Point pB) const
{
	if (!Rules::isPointValid(pA) || !Rules::isPointValid(pB))
		return -1e8;
	double power = 0;
	for (auto point : WuLiner().getPoints(pA, pB))
		power += image.pixels[point.y][point.x] * point.z;
	return power / pA.distTo(pB);
}

vint CubeRestorator::getTop2Angles(v2dbl hough) const
{
	vector<pair<double, int>> angleScores(180);
	for (int th = 0; th<180; ++th)
	{
		angleScores[th].second = th;
		for (auto ry : getTop4Ry(hough, th))
			angleScores[th].first += hough[ry][th];
	}
	sort(angleScores.begin(), angleScores.end());
	reverse(angleScores.begin(), angleScores.end());
	vint top2angles;
	top2angles.push_back(angleScores[0].second);
	for(int i=1; i<180; ++i)
		if(abs(top2angles[0] - angleScores[i].second)>30)
		{
			top2angles.push_back(angleScores[i].second);
			break;
		}
	return top2angles;
}
