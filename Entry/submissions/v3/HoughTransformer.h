#ifndef HOUGH_TRANSFORMER_H
#define HOUGH_TRANSFORMER_H

#include "utils.h"
#include "PGM_Image.h"
#include "math_utils.h"

class HoughTransformer
{
public:
	v2dbl transform(const PGM_Image &image) const
	{
		double cosin[90], sinus[90];
		for(int th=0; th<90; ++th)
		{
			double th_rad = 2*th*PI / 180;
			cosin[th] = cos(th_rad);
			sinus[th] = sin(th_rad);
		}

		int rmax = (int)sqrt(image.height*image.height + image.width*image.width) + 1;
		v2dbl hough(2 * rmax, vdbl(90));
		v2int cnt(2 * rmax, vint(90));
		for(int y = 0; y<image.height; ++y)
			for(int x=0; x<image.width; ++x)
				for(int th=0; th<90; ++th)
				{
					int r = (int)(x*cosin[th] + y*sinus[th]);
					int ry = rmax + r;
					hough[ry][th] += image.pixels[y][x];
					cnt[ry][th]++;
				}
		for (int ry = 0; ry < 2 * rmax; ++ry)
			for (int th = 0; th < 90; ++th)
				if (cnt[ry][th] < 200)
					hough[ry][th] = 0;
				else 
					hough[ry][th] /= cnt[ry][th];
		return hough;
	}
};

#endif
