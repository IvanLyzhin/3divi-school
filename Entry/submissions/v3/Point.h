#ifndef POINT_H
#define POINT_H
#include "PGM_Image.h"
#include <cmath>

struct Point
{
	double x;
	double y;
	double z;

	Point(double x, double y) : x(x), y(y), z(0) {}
	Point(double x, double y, double z) : x(x), y(y), z(z) {}
	
	Point operator+(const Point &p) const
	{
		return Point(x + p.x, y + p.y, z + p.z);
	}

	Point operator-(const Point &p) const
	{
		return Point(x - p.x, y - p.y, z - p.z);
	}

	Point rotate(const v2dbl &rMatrix) const
	{
		return Point(
			x*rMatrix[0][0] + y*rMatrix[1][0] + z*rMatrix[2][0],
			x*rMatrix[0][1] + y*rMatrix[1][1] + z*rMatrix[2][1],
			x*rMatrix[0][2] + y*rMatrix[1][2] + z*rMatrix[2][2]);
	}

	double distTo(const Point &point) const
	{
		double dx = point.x - x;
		double dy = point.y - y;
		double dz = point.z - z;
		return sqrt(dx*dx + dy*dy + dz*dz);
	}

	double distTo(const Point &pointA, const Point &pointB) const
	{
		Point u = pointA - *this;
		Point v = pointB - *this;
		double s = abs(u.x*v.y - u.y*v.x);
		return s / pointA.distTo(pointB);
	}
};

#endif
