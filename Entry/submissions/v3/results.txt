p = 0.0
data/0.0/0.pgm. Error: 3.21636 Time: 833 ms
data/0.0/1.pgm. Error: 3.7499 Time: 822 ms
data/0.0/2.pgm. Error: 5.0 Time: 869 ms
data/0.0/3.pgm. Error: 3.00461 Time: 935 ms
data/0.0/4.pgm. Error: 3.53608 Time: 780 ms
data/0.0/5.pgm. Error: 2.96433 Time: 850 ms
data/0.0/6.pgm. Error: 3.78567 Time: 789 ms
data/0.0/7.pgm. Error: 3.03014 Time: 818 ms
data/0.0/8.pgm. Error: 2.14771 Time: 976 ms
data/0.0/9.pgm. Error: 2.76038 Time: 936 ms
Average error: 3.31951801777

p = 0.1
data/0.1/0.pgm. Error: 2.61964 Time: 907 ms
data/0.1/1.pgm. Error: 2.07913 Time: 908 ms
data/0.1/2.pgm. Error: 2.41208 Time: 849 ms
data/0.1/3.pgm. Error: 5.0 Time: 1048 ms
data/0.1/4.pgm. Error: 2.99036 Time: 910 ms
data/0.1/5.pgm. Error: 5.0 Time: 918 ms
data/0.1/6.pgm. Error: 3.25261 Time: 953 ms
data/0.1/7.pgm. Error: 5.0 Time: 869 ms
data/0.1/8.pgm. Error: 3.44095 Time: 775 ms
data/0.1/9.pgm. Error: 2.33128 Time: 953 ms
Average error: 3.41260740757

p = 0.2
data/0.2/0.pgm. Error: 3.11555 Time: 869 ms
data/0.2/1.pgm. Error: 5.0 Time: 869 ms
data/0.2/2.pgm. Error: 5.0 Time: 804 ms
data/0.2/3.pgm. Error: 5.0 Time: 909 ms
data/0.2/4.pgm. Error: 2.02825 Time: 928 ms
data/0.2/5.pgm. Error: 2.88517 Time: 919 ms
data/0.2/6.pgm. Error: 5.0 Time: 1049 ms
data/0.2/7.pgm. Error: 2.60273 Time: 783 ms
data/0.2/8.pgm. Error: 5.0 Time: 869 ms
data/0.2/9.pgm. Error: 2.87231 Time: 837 ms
Average error: 3.85040204525

p = 0.3
data/0.3/0.pgm. Error: 5.0 Time: 901 ms
data/0.3/1.pgm. Error: 5.0 Time: 982 ms
data/0.3/2.pgm. Error: 5.0 Time: 837 ms
data/0.3/3.pgm. Error: 2.30663 Time: 907 ms
data/0.3/4.pgm. Error: 2.7378 Time: 849 ms
data/0.3/5.pgm. Error: 5.0 Time: 1050 ms
data/0.3/6.pgm. Error: 2.47185 Time: 921 ms
data/0.3/7.pgm. Error: 5.0 Time: 802 ms
data/0.3/8.pgm. Error: 2.79431 Time: 880 ms
data/0.3/9.pgm. Error: 5.0 Time: 909 ms
Average error: 4.031057477

p = 0.4
data/0.4/0.pgm. Error: 3.63533 Time: 875 ms
data/0.4/1.pgm. Error: 3.30609 Time: 836 ms
data/0.4/2.pgm. Error: 4.97327 Time: 918 ms
data/0.4/3.pgm. Error: 2.34928 Time: 878 ms
data/0.4/4.pgm. Error: 5.0 Time: 827 ms
data/0.4/5.pgm. Error: 4.24067 Time: 796 ms
data/0.4/6.pgm. Error: 2.57335 Time: 818 ms
data/0.4/7.pgm. Error: 2.8433 Time: 947 ms
data/0.4/8.pgm. Error: 3.32381 Time: 965 ms
data/0.4/9.pgm. Error: 3.03916 Time: 806 ms
Average error: 3.52842640877

p = 0.5
data/0.5/0.pgm. Error: 1.95432 Time: 944 ms
data/0.5/1.pgm. Error: 3.88151 Time: 864 ms
data/0.5/2.pgm. Error: 5.0 Time: 830 ms
data/0.5/3.pgm. Error: 5.0 Time: 886 ms
data/0.5/4.pgm. Error: 2.69397 Time: 762 ms
data/0.5/5.pgm. Error: 1.85657 Time: 826 ms
data/0.5/6.pgm. Error: 5.0 Time: 856 ms
data/0.5/7.pgm. Error: 4.16708 Time: 863 ms
data/0.5/8.pgm. Error: 5.0 Time: 987 ms
data/0.5/9.pgm. Error: 2.39662 Time: 761 ms
Average error: 3.69500778913

p = 0.6
data/0.6/0.pgm. Error: 2.54501 Time: 994 ms
data/0.6/1.pgm. Error: 5.0 Time: 900 ms
data/0.6/2.pgm. Error: 5.0 Time: 844 ms
data/0.6/3.pgm. Error: 5.0 Time: 766 ms
data/0.6/4.pgm. Error: 3.82825 Time: 767 ms
data/0.6/5.pgm. Error: 5.0 Time: 853 ms
data/0.6/6.pgm. Error: 2.33023 Time: 990 ms
data/0.6/7.pgm. Error: 5.0 Time: 856 ms
data/0.6/8.pgm. Error: 3.74095 Time: 825 ms
data/0.6/9.pgm. Error: 5.0 Time: 769 ms
Average error: 4.24444437027

p = 0.7
data/0.7/0.pgm. Error: 5.0 Time: 945 ms
data/0.7/1.pgm. Error: 2.84632 Time: 807 ms
data/0.7/2.pgm. Error: 1.72455 Time: 824 ms
data/0.7/3.pgm. Error: 2.42998 Time: 913 ms
data/0.7/4.pgm. Error: 5.0 Time: 952 ms
data/0.7/5.pgm. Error: 5.0 Time: 981 ms
data/0.7/6.pgm. Error: 5.0 Time: 881 ms
data/0.7/7.pgm. Error: 5.0 Time: 886 ms
data/0.7/8.pgm. Error: 5.0 Time: 846 ms
data/0.7/9.pgm. Error: 5.0 Time: 935 ms
Average error: 4.2000849843

p = 0.8
data/0.8/0.pgm. Error: 5.0 Time: 801 ms
data/0.8/1.pgm. Error: 5.0 Time: 1023 ms
data/0.8/2.pgm. Error: 5.0 Time: 824 ms
data/0.8/3.pgm. Error: 5.0 Time: 960 ms
data/0.8/4.pgm. Error: 5.0 Time: 977 ms
data/0.8/5.pgm. Error: 5.0 Time: 983 ms
data/0.8/6.pgm. Error: 5.0 Time: 883 ms
data/0.8/7.pgm. Error: 5.0 Time: 819 ms
data/0.8/8.pgm. Error: 5.0 Time: 825 ms
data/0.8/9.pgm. Error: 5.0 Time: 828 ms
Average error: 5.0

p = 0.9
data/0.9/0.pgm. Error: 5.0 Time: 838 ms
data/0.9/1.pgm. Error: 5.0 Time: 885 ms
data/0.9/2.pgm. Error: 5.0 Time: 857 ms
data/0.9/3.pgm. Error: 5.0 Time: 866 ms
data/0.9/4.pgm. Error: 5.0 Time: 897 ms
data/0.9/5.pgm. Error: 5.0 Time: 1010 ms
data/0.9/6.pgm. Error: 5.0 Time: 846 ms
data/0.9/7.pgm. Error: 5.0 Time: 782 ms
data/0.9/8.pgm. Error: 5.0 Time: 1069 ms
data/0.9/9.pgm. Error: 5.0 Time: 838 ms
Average error: 5.0

Total error: 4.02815485001