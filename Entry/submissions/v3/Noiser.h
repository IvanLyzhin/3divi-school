#ifndef NOISER_H
#define NOISER_H
#include "PGM_Image.h"
#include <random>
#include <ctime>

class Noiser
{
	double p;

public:
	Noiser(double p) : p(p) {}

	void noise(PGM_Image &image)
	{
		uniform_real_distribution<> distr;
		default_random_engine engine(time(nullptr));
		for (int y = 0; y < image.height; ++y)
			for (int x = 0; x < image.width; ++x)
				if (distr(engine) < p)
					image.pixels[y][x] = distr(engine) * 256;
	}
};

#endif
