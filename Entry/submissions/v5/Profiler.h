#ifndef PROFILER_H
#define PROFILER_H

class Profiler
{
private:
	static double prevCheckPointTime;

public:
	static void checkpoint(char *label);
};

#endif
