#include "Randomizer.h"
#include <ctime>

default_random_engine Randomizer::engine = default_random_engine(time(NULL));

int Randomizer::getNextInt(int min, int max)
{
	uniform_int_distribution<> distr(min, max);
	return distr(engine);
}

double Randomizer::getNextDouble(int min, int max)
{
	uniform_real_distribution<> distr(min, max);
	return distr(engine);
}

