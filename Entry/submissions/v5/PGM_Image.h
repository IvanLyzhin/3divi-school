#ifndef PGM_IMAGE_H
#define PGM_IMAGE_H

#include "utils.h"
#include <cstdio>

struct PGM_Image
{
	int height;
	int width;
	v2byte pixels;

	PGM_Image(int height, int width) : height(height), width(width), pixels(v2byte(height, vbyte(width))) {}

	static PGM_Image fromFile(char *filename)
	{
		FILE *file = fopen(filename, "rb");
		fscanf(file, "P5\n");
		int w, h;
		fscanf(file, "%d %d\n", &w, &h);
		fscanf(file, "255\n");
		PGM_Image image(w, h);
		for (int y = 0; y < h; ++y)
			for (int x = 0; x < w; ++x)
				fscanf(file, "%c", &image.pixels[y][x]);
		fclose(file);
		return image;
	}

	void saveToFile(char *filename)
	{
		FILE *file = fopen(filename, "wb");
		fprintf(file, "P5\n");
		fprintf(file, "%d %d\n", width, height);
		fprintf(file, "255\n");
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
				fprintf(file, "%c", pixels[y][x]);
		fclose(file);
	}
};

#endif
