#include <cstdio>
#include <string>
#include "PGM_Image.h"
#include "Point.h"
#include "CubeGenerator.h"
#include "Rules.h"
#include "WuLiner.h"
#include "Noiser.h"
#include "CubeRestorator.h"
#include "Profiler.h"
using namespace std;

void generate(double p)
{
	printf("Generating with P=%.1f...\n", p);
	vector<Point> vertices = CubeGenerator().generateCube();
	printf("Generated vertices: \n");
	for (auto v : vertices)
		printf("%.3f %.3f\n", v.x, v.y);
	printf("Drawing cube on image...\n");
	PGM_Image image(Rules::imageHeight, Rules::imageWidth);
	for (int i = 0; i < 4; ++i)
	{
		WuLiner::drawLine(image, vertices[i], vertices[(i + 1) % 4]);
		WuLiner::drawLine(image, vertices[i + 4], vertices[(i + 1) % 4 + 4]);
		WuLiner::drawLine(image, vertices[i], vertices[i + 4]);
	}
	printf("Noising...\n");
	Noiser(p).noise(image);
	printf("Saving to image.pgm...\n");
	image.saveToFile("image.pgm");
	printf("Done\n");
}

void restore(char *filename)
{
	printf("Restoring cube from file %s\n", filename);
	PGM_Image image = PGM_Image::fromFile(filename);
	vector<Point> vertices = CubeRestorator().restoreCube(image);
	printf("Restored vertices: \n");
	for (auto v : vertices)
		printf("%.3f %.3f\n", v.x, v.y);
	printf("Saving vertices to output.txt...\n");
	FILE *file = fopen("output.txt", "wb");
	for (auto v : vertices)
		fprintf(file, "%.3f %.3f\n", v.x, v.y);
	fclose(file);
#ifdef DEBUG
	printf("Drawing cube...\n");
	PGM_Image resultImage(Rules::imageHeight, Rules::imageWidth);
	for (int i = 0; i<4; ++i)
	{
		WuLiner::drawLine(resultImage, vertices[i], vertices[i + 4]);
		WuLiner::drawLine(resultImage, vertices[i], vertices[(i + 1) % 4]);
		WuLiner::drawLine(resultImage, vertices[i + 4], vertices[(i + 1) % 4 + 4]);
	}
	resultImage.saveToFile("result.pgm");
#endif
	printf("Done\n");
	Profiler::checkpoint("End restoring");
}

void help()
{
	printf("Usage: solver -generate P | solver -restore image.pgm\n");
}

int main(int argc, char **argv)
{
	if (argc < 3)
		help();
	else
	{
		string command(argv[1]);
		if (command == "-generate")
			generate(atof(argv[2]));
		else if (command == "-restore")
			restore(argv[2]);
		else
			help();
	}
	return 0;
}