class Point:
	def __init__(self, x, y):
		self.x = x
		self.y = y
		
	def __str__(self):
		return '({}, {})'.format(self.x, self.y)
		
	def equal(self, other):
		return self.x == other.x and self.y == other.y
		
	def equal_eps(self, other, eps):
		return abs(self.x-other.x)<eps and abs(self.y-other.y)<eps
		
	def add(self, other):
		return Point(self.x + other.x, self.y + other.y)
		
	def sub(self, other):
		return Point(self.x - other.x, self.y - other.y)
		
	def is_valid(self, image_shape):
		return self.x>=0 and self.y>=0 and self.x<image_shape[0] and self.y<image_shape[1]
