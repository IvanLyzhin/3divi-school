"""
    Finding most top, left and right points
"""

from utils.data_reader import DataReader
from os.path import join
import pandas as pd
import numpy as np
import utils.points_helpers as ph
import utils.show_helpers as sh
import utils.metrics_helpers as mh
import cv2

data_dir = 'data'


def get_most_top(mask):
    for y in range(mask.shape[0]):
        for x in range(mask.shape[1]):
            if mask[y][x]:
                return x, y


def get_most_bottom(mask):
    for y in range(mask.shape[0]-1, -1, -1):
        for x in range(mask.shape[1]):
            if mask[y][x]:
                return x, y


def detect_head(depth, mask):
    x, y = get_most_top(mask)
    most_top = ph.back_project([x, y, depth[y, x]])
    x, y = get_most_bottom(mask)
    most_bottom = ph.back_project([x, y, depth[y, x]])
    height = most_top[1] - most_bottom[1]
    head = np.add(most_top, [0, -height / 10, 100])
    return head


def get_near_points(depth, mask, point, radius):
    near_points = []
    for y in range(mask.shape[0]):
        for x in range(mask.shape[1]):
            if mask[y, x]:
                cur_point = ph.back_project([x, y, depth[y, x]])
                if np.linalg.norm(np.subtract(point, cur_point)) < radius:
                    near_points.append(cur_point)
    return np.array(near_points)


def detect_right_wrist(depth, mask):
    most_left = None
    for x in range(mask.shape[1]):
        for y in range(mask.shape[0]):
            if mask[y][x]:
                most_left = [x, y, depth[y, x]]
                break
        if most_left:
            break
    near_points = get_near_points(depth, mask, ph.back_project(most_left), 250)
    mean, eig = cv2.PCACompute(near_points, None, cv2.PCA_DATA_AS_ROW)
    return mean[0]


def detect_left_wrist(depth, mask):
    most_right = None
    for x in range(mask.shape[1]-1, -1, -1):
        for y in range(mask.shape[0]):
            if mask[y][x]:
                most_right = [x, y, depth[y, x]]
                break
        if most_right:
            break
    near_points = get_near_points(depth, mask, ph.back_project(most_right), 250)
    mean, eig = cv2.PCACompute(near_points, None, cv2.PCA_DATA_AS_ROW)
    return mean[0]


def cut_by_knees(mask):
    cutted_mask = np.copy(mask)
    _, y_top = get_most_top(mask)
    _, y_bottom = get_most_bottom(mask)
    y_knee = y_bottom - (y_bottom - y_top) * 0.25
    for y in range(int(y_knee), y_bottom+1):
        for x in range(mask.shape[1]):
            cutted_mask[y, x] = 0
    return cutted_mask


def solve(depth, mask, rgb):
    mask = cut_by_knees(mask)
    sh.show_gray(mask)
    head = detect_head(depth, mask)
    wrist_l = detect_left_wrist(depth, mask)
    wrist_r = detect_right_wrist(depth, mask)

    ans = np.array([head, wrist_l, wrist_r], dtype=np.float32)
    return ans


def main():
    sh.enable_visualization()
    dr = DataReader(join(data_dir, 'test1'))

    scores = []
    # while dr.move_next():
    for _ in range(1):
        dr.move_next()
        depth, mask, rgb = dr.get_data()
        ans = solve(depth, mask, rgb)
        true_ans = dr.get_ans()
        print('ANS: {}'.format(ans))
        sh.show_ans(rgb, ans)
        scores.append(mh.calc_score(true_ans, ans, verbose=True))
    print('Average score: {}'.format(np.average(scores)))


if __name__ == "__main__":
    main()
