import argparse
import time

from utils.data_reader import DataReader
from utils.metrics_helpers import calc_score


def load_solver(module_name):
    try:
        module = __import__(module_name)
        solver_class = getattr(module, "Solver")
        solver = solver_class()
        return solver
    except ModuleNotFoundError:
        print('Module {} not found'.format(module_name))
        exit(1)
    except AttributeError:
        print('Module {} does not contains class Solver'.format(module_name))
        exit(1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--module', help='module name to test')
    parser.add_argument('--folder', help='folder with test data')
    parser.add_argument('--verbose', help='print score for each image', action='store_const', const=True, default=False)
    args = parser.parse_args()
    print('Module:', args.module)
    print('Folder:', args.folder)

    solver = load_solver(args.module)
    dr = DataReader(args.folder)
    score = 0
    start = time.time()
    while dr.move_next():
        print(dr.get_filename())
        ans = solver.process(dr.get_data())
        true_ans = dr.get_ans()
        score += calc_score(true_ans, ans, verbose=args.verbose)
    max_score = 3 * dr.get_length()
    end = time.time()
    total_time = end - start
    print("Total score: {}/{} ({:.2f}%)".format(score, max_score, score / max_score * 100))
    print("Total time: {:.2f} (~{:.2f} per image)".format(total_time, total_time / dr.get_length()))


if __name__ == "__main__":
    main()
