"""
    Clustering all points to groups, building graph and finding leafs or special patterns for head
"""

from utils.data_reader import DataReader
import utils.metrics_helpers as mh
import utils.show_helpers as sh
import utils.points_helpers as ph
import numpy as np
import algorithms.mosaic_builders as mb


def detect_head(graph, centers):
    n_vertices = len(centers)
    head_vertex = None
    # TODO добавить еще один паттерн головы
    # TODO попробовать другой метод кластеризации http://scikit-learn.org/stable/modules/clustering.html
    for i in range(n_vertices):
        deg = np.count_nonzero(graph[i])
        if deg == 2:
            neigh = np.nonzero(graph[i])[0]
            deg1 = np.count_nonzero(graph[neigh[0]])
            deg2 = np.count_nonzero(graph[neigh[1]])
            if deg1 > deg2:
                deg1, deg2 = deg2, deg1
                neigh[0], neigh[1] = neigh[1], neigh[0]
            pattern1 = deg1 > 2 and deg2 > 2
            pattern2 = deg1 == 2 and graph[neigh[0], neigh[1]]
            if pattern1 or pattern2:
                if (head_vertex is None) or (centers[head_vertex][1] > centers[i][1]):
                    head_vertex = i
    if head_vertex is not None:
        neigh = np.nonzero(graph[head_vertex])[0]
        deg1 = np.count_nonzero(graph[neigh[0]])
        deg2 = np.count_nonzero(graph[neigh[1]])
        return np.mean([centers[head_vertex], centers[neigh[0]], centers[neigh[1]]], axis=0)
    print('!!! HEAD PATTERN NOT FOUND !!!')
    return centers[0]


def solve(depth, mask, rgb):
    ans = {}
    n_clusters = 60
    mosaic, centers = mb.build_mosaic_kmeans(mask, n_clusters)
    mosaic_img = mb.build_mosaic_img(mosaic)
    # sh.show_rgb(mosaic_img)
    graph = np.zeros((n_clusters, n_clusters), dtype=np.bool)
    height, width = mask.shape
    for y1 in range(height):
        for x1 in range(width):
            if not mask[y1, x1]:
                continue
            for y2 in range(y1 - 1, y1 + 2):
                for x2 in range(x1 - 1, x1 + 2):
                    if x2 < 0 or y2 < 0 or x2 >= mask.shape[1] or y2 >= mask.shape[0] or not mask[y2][x2]:
                        continue
                    if mosaic[y1, x1] != mosaic[y2, x2]:
                        m1, m2 = mosaic[y1, x1], mosaic[y2, x2]
                        graph[m1, m2] = graph[m2, m1] = True
    sh.show_graph(mosaic_img, centers, graph)

    deg = [np.count_nonzero(graph[i]) for i in range(n_clusters)]
    leafs = []
    min_y = np.min(np.nonzero(mask)[0])
    max_y = np.max(np.nonzero(mask)[0])
    thresh_y = max_y - 0.2 * (max_y - min_y)
    for i in range(n_clusters):
        if deg[i] == 1 and centers[i][1] < thresh_y:
            leafs.append(i)
            sh.draw_circle(mosaic_img, centers[i], 10, (0, 0, 0))
    # sh.show_rgb(mosaic_img)

    leafs_len = []
    for i in range(len(leafs)):
        length = 0
        par = -1
        u = leafs[i]
        while deg[u] < 3:
            length += 1
            vs = np.nonzero(graph[u])[0]
            v = None
            if vs[0] != par:
                v = vs[0]
            else:
                v = vs[1]
            par, u = u, v
        leafs_len.append(length)
    for i in range(len(leafs)):
        for j in range(i + 1, len(leafs)):
            if leafs_len[i] > leafs_len[j] or (
                    leafs_len[i] == leafs_len[j] and centers[leafs[i]][1] > centers[leafs[j]][1]):
                leafs_len[i], leafs_len[j] = leafs_len[j], leafs_len[i]
                leafs[i], leafs[j] = leafs[j], leafs[i]
    if len(leafs) == 3:
        head2 = ph.int_tuple2(np.mean([centers[leafs[0]], centers[np.nonzero(graph[leafs[0]])[0][0]]], axis=0))
        wristL2 = ph.int_tuple2(np.mean([centers[leafs[1]], centers[np.nonzero(graph[leafs[1]])[0][0]]], axis=0))
        wristR2 = ph.int_tuple2(np.mean([centers[leafs[2]], centers[np.nonzero(graph[leafs[2]])[0][0]]], axis=0))
    elif len(leafs) == 2:
        head2 = ph.int_tuple2(detect_head(graph, centers))
        wristL2 = ph.int_tuple2(np.mean([centers[leafs[0]], centers[np.nonzero(graph[leafs[0]])[0][0]]], axis=0))
        wristR2 = ph.int_tuple2(np.mean([centers[leafs[1]], centers[np.nonzero(graph[leafs[1]])[0][0]]], axis=0))
    else:
        print('SOMETHING GOES WRONG')
        head2 = ph.int_tuple2(centers[0])
        wristL2 = ph.int_tuple2(centers[1])
        wristR2 = ph.int_tuple2(centers[2])
    if wristL2[0] < wristR2[0]:
        wristL2, wristR2 = wristR2, wristL2
    sh.draw_circle(rgb, head2, 10, (255, 0, 0))
    sh.draw_circle(rgb, wristL2, 10, (0, 255, 0))
    sh.draw_circle(rgb, wristR2, 10, (0, 0, 255))
    sh.show_rgb(rgb)
    sh.show_rgb(mosaic_img)
    head = ph.back_project((head2[0], head2[1], depth[head2[1], head2[0]] + 100))
    wrist_l = ph.back_project((wristL2[0], wristL2[1], depth[wristL2[1], wristL2[0]]))
    wrist_r = ph.back_project((wristR2[0], wristR2[1], depth[wristR2[1], wristR2[0]]))
    return np.array([head, wrist_l, wrist_r], dtype=np.float32)


def main():
    sh.enable_visualization()
    dr = DataReader('data/test2')
    scores = []
    try:
        #sh.disable_visualization()
        # dr.move_to("0000108.png")
        #while dr.move_next():
        for _ in range(5):
            dr.move_next()
            print(dr.get_filename())
            depth, mask, rgb = dr.get_data()
            ans = solve(depth, mask, rgb)
            true_ans = dr.get_ans()
            # sh.show_ans(rgb, ans, true_ans)
            score = mh.calc_score(true_ans, ans, verbose=True)
            scores.append(score)
    finally:
        print("Average Score: {}".format(np.mean(scores)))


if __name__ == '__main__':
    main()
