import random

random.seed(0)


def __gen():
    return random.randint(0, 255)


def get_color():
    return (__gen(), __gen(), __gen())


def get_colors(n_colors):
    return [get_color() for i in range(n_colors)]
