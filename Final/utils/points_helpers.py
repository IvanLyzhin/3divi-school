import cv2
import numpy as np


def int_tuple2(p):
    return int(p[0]), int(p[1])


def find_closest_nonzero(start, depth):
    start = int_tuple2(start)
    q = [start]
    h = 0
    used = {start: True}
    while True:
        x, y = q[h]
        h += 1
        if depth[y, x] > 0:
            return x, y
        for dy in range(-1, 2):
            for dx in range(-1, 2):
                p = (x+dx, y+dy)
                if p not in used:
                    used[p] = True
                    q.append(p)


def back_project(point):
    x_proj, y_proj, z_proj = point
    x_real = (x_proj - 320) / 575.7 * z_proj
    y_real = (240 - y_proj) / 575.7 * z_proj
    z_real = z_proj
    return np.array([x_real, y_real, z_real])


def project(point):
    x_real, y_real, z_real = point
    x_proj = 320 + x_real / z_real * 575.7
    y_proj = 240 - y_real / z_real * 575.7
    z_proj = z_real
    return np.array([x_proj, y_proj, z_proj])


def get_neighbours(point, shape):
    neighbours = []
    for x in range(point[0]-1, point[0]+2):
        for y in range(point[1]-1, point[1]+2):
            if 0 <= x <= shape[1] and 0 <= y <= shape[0]:
                neighbours.append([x, y])
    return neighbours


def rotate_contour(contour, center, angle):
    rot_matrix = cv2.getRotationMatrix2D(tuple(center), angle, 1)
    ones = np.ones(shape=(len(contour), 1))
    contour = np.hstack((contour, ones))
    rotated_contour = np.transpose(np.dot(rot_matrix, np.transpose(contour)))
    rotated_contour = np.array([int_tuple2(p) for p in rotated_contour])
    return rotated_contour


def is_inside_rectangle(rect_contour, point):
    area1 = cv2.contourArea(rect_contour)
    area2 = 0
    for i in range(4):
        j = (i + 1) % 4
        area2 += cv2.contourArea(np.array([rect_contour[i], rect_contour[j], point]))
    return abs(area1 - area2) < 10

def move_by_real_dist(point, dist):
    real_point = back_project(point)
    real_point += dist
    return project(real_point)
