import numpy as np


def calc_score(expected_ans, actual_ans, verbose=False):
    head_error = np.linalg.norm(np.subtract(expected_ans[0], actual_ans[0]))
    wrist_l_error = np.linalg.norm(np.subtract(expected_ans[1], actual_ans[1]))
    wrist_r_error = np.linalg.norm(np.subtract(expected_ans[2], actual_ans[2]))
    errors = [head_error, wrist_l_error, wrist_r_error]
    score = np.count_nonzero(np.less(errors, 100))
    if verbose:
        print('Errors: {}'.format(errors))
        print('Score: {}/3'.format(score))

    return score
