import matplotlib.pyplot as plt
import numpy as np
import cv2
import utils.points_helpers as ph

__enabled = False


def enable_visualization():
    global __enabled
    __enabled = True


def disable_visualization():
    global __enabled
    __enabled = False


def __show(img, cmap, title=None):
    if not __enabled:
        return
    plt.figure()
    plt.imshow(img, cmap=cmap)
    if title:
        plt.title(title)
    plt.show()


def show_gray(img, title=None):
    __show(img, 'gray', title)


def show_rgb(img, title=None):
    __show(img, None, title)


def draw_circle(img, center, radius, color):
    center = ph.int_tuple2(center)
    cv2.circle(img, center, radius, color, 1, cv2.LINE_AA)


def draw_circle_real(img, center, radius, color):
    center = ph.project(center)
    draw_circle(img, center, radius, color)


def show_ans(img, ans, true_ans=None):
    img = np.copy(img)
    # head
    draw_circle_real(img, ans[0], 10, (255, 0, 0))
    proj = ph.project(ans[0])
    cv2.putText(img, "H", (int(proj[0]), int(proj[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    # left wrist
    draw_circle_real(img, ans[1], 10, (0, 255, 0))
    proj = ph.project(ans[1])
    cv2.putText(img, "L", (int(proj[0]), int(proj[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    # right wrist
    draw_circle_real(img, ans[2], 10, (0, 0, 255))
    proj = ph.project(ans[2])
    cv2.putText(img, "R", (int(proj[0]), int(proj[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

    if true_ans is not None:
        draw_circle_real(img, true_ans[0], 5, (255, 0, 0))
        draw_circle_real(img, true_ans[1], 5, (0, 255, 0))
        draw_circle_real(img, true_ans[2], 5, (0, 0, 255))

    show_rgb(img, 'Result')


def show_graph(img, centers, graph):
    n = len(centers)
    for i in range(n):
        for j in range(i + 1, n):
            if graph[i][j]:
                cv2.line(img, ph.int_tuple2(centers[i]), ph.int_tuple2(centers[j]), (0, 0, 0), 2, cv2.LINE_AA)
    show_rgb(img)


def __show_real_points(points, proj):
    letter2index = {'X': 0, 'Y': 1, 'Z': 2}
    lims = {'X': (-1000, 1000), 'Y': (-1000, 1000), 'Z': (1800, 2600)}
    xs = points[:, letter2index[proj[0]]]
    ys = points[:, letter2index[proj[1]]]
    plt.scatter(xs, ys, c='r', marker='.')
    plt.xlabel(proj[0])
    plt.ylabel(proj[1])
    plt.xlim(lims[proj[0]])
    plt.ylim(lims[proj[1]])


def show_points(points, proj='XY'):
    real_points = np.array([ph.back_project(p) for p in points])
    plt.figure()
    __show_real_points(real_points, proj)
    plt.show()


def show_points_3d_proj(points):
    real_points = np.array([ph.back_project(p) for p in points])
    plt.figure()
    plt.subplot(221)
    __show_real_points(real_points, proj='XY')
    plt.subplot(222)
    __show_real_points(real_points, proj='ZY')
    plt.subplot(223)
    __show_real_points(real_points, proj='XZ')
    plt.show()


def __show_real_graph_3d_proj(real_centers, graph, proj):
    letter2index = {'X': 0, 'Y': 1, 'Z': 2}
    lims = {'X': (-1000, 1000), 'Y': (-1000, 1000), 'Z': (1800, 2600)}
    ax1 = letter2index[proj[0]]
    ax2 = letter2index[proj[1]]
    xs = real_centers[:, ax1]
    ys = real_centers[:, ax2]
    plt.xlabel(proj[0])
    plt.ylabel(proj[1])
    plt.xlim(lims[proj[0]])
    plt.ylim(lims[proj[1]])
    plt.scatter(xs, ys, c='r', marker='.')
    for i in range(len(real_centers)):
        for j in range(i+1, len(real_centers)):
            if graph[i, j]:
                plt.plot([real_centers[i, ax1], real_centers[j, ax1]], [real_centers[i, ax2], real_centers[j, ax2]])


def show_graph_3d_proj(centers, graph):
    real_centers = np.array([ph.back_project(center) for center in centers])
    plt.figure(figsize=(20, 20))
    plt.subplot(221)
    __show_real_graph_3d_proj(real_centers, graph, proj='XY')
    plt.subplot(222)
    __show_real_graph_3d_proj(real_centers, graph, proj='ZY')
    plt.subplot(223)
    __show_real_graph_3d_proj(real_centers, graph, proj='XZ')
    plt.show()
    pass
