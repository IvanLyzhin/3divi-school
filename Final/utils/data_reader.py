import numpy as np
import pandas as pd
import cv2
import os
from os.path import join


class DataReader:
    def __init__(self, dir, with_ans=True):
        self.dir = dir
        self.filenames = os.listdir(os.path.join(dir, "Depth"))
        self.index = -1
        self.count = len(self.filenames)
        if with_ans:
            self.ans = pd.read_csv(join(dir, "data.csv"), index_col=False)

    def get_length(self):
        return len(self.filenames)

    def move_next(self):
        if self.index + 1 == self.count:
            return False
        self.index += 1
        return True

    def move_to(self, filename):
        for i in range(len(self.filenames)):
            if self.filenames[i] == filename:
                self.index = i
                return True
        return False

    def get_data(self):
        filename = self.filenames[self.index]
        depth = cv2.imread(os.path.join(self.dir, "Depth", filename), cv2.IMREAD_UNCHANGED)
        mask = cv2.imread(os.path.join(self.dir, "Mask", filename), cv2.IMREAD_GRAYSCALE)
        rgb = cv2.imread(os.path.join(self.dir, "RGB", filename), cv2.IMREAD_COLOR)

        rgb = cv2.cvtColor(rgb, cv2.COLOR_BGR2RGB)

        return depth, mask, rgb

    def get_ans(self):
        ans = {}
        row = self.ans.iloc[self.index]
        head = [row['head.x'], row['head.y'], row['head.z']]
        wristL = [row['wrist.L.x'], row['wrist.L.y'], row['wrist.L.z']]
        wristR = [row['wrist.R.x'], row['wrist.R.y'], row['wrist.R.z']]
        ans = np.array([head, wristL, wristR], dtype=np.float32)
        return ans

    def get_filename(self):
        return self.filenames[self.index]
