"""
    Finding skin on rgb image, clustering, haar
"""

import numpy as np
import cv2
from sklearn.cluster import KMeans
from cv2.ximgproc import createSuperpixelSLIC

from utils.data_reader import DataReader
import utils.metrics_helpers as mh
import utils.show_helpers as sh
import utils.points_helpers as ph
import algorithms.mosaic_builders as mosaics
from detectors.head import detect_by_haar
import os

skin_hist = None


def init():
    print('Start training')
    global skin_hist
    skin_folder = 'data/train/skin'
    for filename in os.listdir(skin_folder):
        img = cv2.imread(os.path.join(skin_folder, filename))
        train_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        train_mask = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        hist = cv2.calcHist([train_hsv], [0, 1], train_mask, [180, 256], [0, 180, 0, 256])
        print(type(hist))
        print(hist.shape)
        if skin_hist is None:
            skin_hist = hist
        else:
            skin_hist += hist
    skin_hist = cv2.normalize(skin_hist, skin_hist, 0, 1, cv2.NORM_MINMAX)
    print('End training')


def solve(depth, mask, rgb):
    ans = {'head': [1, 1, 1], 'wrist.L': [1, 1, 1], 'wrist.R': [1, 1, 1]}

    hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV).astype(np.float32)
    skin_prob = cv2.calcBackProject([hsv], [0, 1], skin_hist, [0, 180, 0, 256], 1)
    skin_prob[~mask] = 0
    sh.show_gray(skin_prob, 'Skin probability')

    slic = createSuperpixelSLIC(hsv[:, :, :2], region_size=15)
    slic.iterate()
    print(np.count_nonzero(slic.getLabelContourMask()))
    contour_mask = slic.getLabelContourMask().astype(np.bool)
    mosaic = rgb.copy()
    mosaic[contour_mask] = np.array([0, 0, 0])
    sh.show_rgb(mosaic)

    num = slic.getNumberOfSuperpixels()
    labels = slic.getLabels()
    probs_by_cluster = [[] for _ in range(num)]
    for i in range(skin_prob.shape[0]):
        for j in range(skin_prob.shape[1]):
            probs_by_cluster[labels[i, j]].append(skin_prob[i, j])
    median_probs = [np.mean(probs_by_cluster[c]) for c in range(num)]
    for i in range(skin_prob.shape[0]):
        for j in range(skin_prob.shape[1]):
            if mask[i, j]:
                skin_prob[i, j] = median_probs[labels[i, j]]
    thresh = 0.001
    skin_mask = skin_prob > thresh
    skin_img = rgb.copy()
    skin_img[~skin_mask] = [0, 0, 0]
    skin_prob[~skin_mask] = 0
    sh.show_rgb(skin_img)

    mosaic, centers = mosaics.build_mosaic_dbscan(skin_prob)
    mosaic_img = mosaics.build_mosaic_img(mosaic)
    sh.show_rgb(mosaic_img)

    head = detect_by_haar(rgb, mask)
    if head:
        ans['head'] = ph.back_project([head[0], head[1], depth[int(head[1]), int(head[0])] + 100])
    else:
        print('Head not found')

    return ans


def main():
    dr = DataReader('data/test1')
    scores = []
    try:
        #dr.move_to("0000114.png")
        #while dr.move_next():
        for _ in range(1):
            dr.move_next()
            print(dr.get_filename())
            depth, mask, rgb = dr.get_data()
            ans = solve(depth, mask, rgb)
            true_ans = dr.get_ans()
            #sh.show_ans(rgb, ans, true_ans)
            score = mh.calc_score(true_ans, ans, verbose=True)
            scores.append(score)
    finally:
        print('Average score: ', np.mean(scores))


if __name__ == '__main__':
    init()
    main()
