"""

"""

import time
import numpy as np
import cv2

from utils.data_reader import DataReader
import utils.metrics_helpers as mh
import utils.points_helpers as ph
import utils.show_helpers as sh
from detectors.head import detect_by_haar
import algorithms.ransac as ransac
from math import hypot, sqrt, acos
import math

# TODO рассматривать в первую очередь углы с предыдущего кадра
# TODO выражать размеры в метрических координатах


def angle_between_vectors(u, v):
    cs = np.dot(u, v) / np.linalg.norm(u) / np.linalg.norm(v)
    cs = max(cs, -1.0 + 1e-6)
    cs = min(cs, 1.0 - 1e-6)
    sn = u[0] * v[1] - u[1] * v[0]
    angle = -acos(cs) * 180 / math.pi
    if sn < 0:
        angle = -angle
    return angle


def find_rect_rotation_center(rect_contour, min_angle, max_angle, points):
    bins = [0 for _ in range(180)]
    center = np.mean([rect_contour[0], rect_contour[2]], axis=0)
    w = np.linalg.norm(rect_contour[2] - rect_contour[1])
    h = np.linalg.norm(rect_contour[1] - rect_contour[0])
    for p in points:
        dist = np.linalg.norm(p - center)
        if dist <= w/2:
            pass
        elif dist <= h/2:
            a = sqrt(dist**2 - (w/2)**2)
            q = np.mean([rect_contour[2], rect_contour[3]], axis=0) + (rect_contour[2]-rect_contour[3]) * a / h
            u = q - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            alpha = round(alpha)
            if alpha < -90:
                alpha += 180
            if alpha >= 90:
                alpha -= 180
            delta = acos((2*dist**2 - w**2)/(2*dist**2)) * 180 / math.pi
            beta = round(alpha + delta)
            if beta < -90:
                beta += 180
            if beta >= 90:
                beta -= 180
            if alpha <= beta:
                bins[alpha + 90] += 1
                bins[beta + 90] -= 1
            else:
                bins[alpha + 90] += 1
                bins[179] -= 1
                bins[0] += 1
                bins[beta + 90] -= 1
    best_support = 0
    best_angle = 0
    support = 0
    for angle in range(-90, 90):
        support += bins[angle + 90]
        if support > best_support and min_angle <= angle <= max_angle:
            best_support = support
            best_angle = angle
    return best_angle


def find_rect_rotaion_top(rect_contour, min_angle, max_angle, points, rgb=None):
    bins = [0 for _ in range(360)]
    bin_points = [[] for _ in range(360)]
    taked = [False for _ in points]
    center = np.mean([rect_contour[0], rect_contour[3]], axis=0)
    w = np.linalg.norm(rect_contour[2] - rect_contour[1])
    h = np.linalg.norm(rect_contour[1] - rect_contour[0])

    def add_segment(alpha, beta, i):
        def norm(a):
            if a < -180:
                a += 360
            if a >= 180:
                a -= 360
            return a
        alpha = norm(round(alpha))
        beta = norm(round(beta))
        if alpha <= beta:
            bins[alpha + 180] += 1
            bin_points[alpha + 180].append(i)
            bins[beta + 180] -= 1
            bin_points[beta + 180].append(-i)
        else:
            bins[alpha + 180] += 1
            bin_points[alpha + 180].append(i)
            bins[359] -= 1
            bin_points[359].append(-i)
            bins[0] += 1
            bin_points[0].append(i)
            bins[beta + 180] -= 1
            bin_points[beta + 180].append(-i)

    r = hypot(h, w/2)
    for i, p in enumerate(points):
        dist = np.linalg.norm(p - center)
        if dist < 1e-6:
            pass
        elif dist <= w / 2:
            u = rect_contour[3] - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            beta = alpha + 180
            add_segment(alpha, beta, i + 1)
        elif dist <= h:
            a = sqrt(dist ** 2 - (w / 2) ** 2)
            q = rect_contour[3] + (rect_contour[2] - rect_contour[3]) * a / h
            u = q - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            delta = acos((2 * dist ** 2 - w ** 2) / (2 * dist ** 2)) * 180 / math.pi
            beta = alpha + delta
            add_segment(alpha, beta, i + 1)
        elif dist < r:
            a = sqrt(dist ** 2 - (w / 2) ** 2)
            q = rect_contour[3] + (rect_contour[2] - rect_contour[3]) * a / h
            u = q - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            b = w/2 - sqrt(dist**2 - h**2)
            c = hypot(h - a, b)
            delta1 = acos((2 * dist ** 2 - c ** 2) / (2 * dist ** 2)) * 180 / math.pi
            delta = acos((2 * dist ** 2 - w ** 2) / (2 * dist ** 2)) * 180 / math.pi
            beta = alpha + delta
            add_segment(alpha, alpha + delta1, i + 1)
            add_segment(beta - delta1, beta, i + 1)

    best_support = 0
    best_angle = 0
    support = 0
    for angle in range(-180, 180):
        support += bins[angle + 180]
        for i in bin_points[angle + 180]:
            if i > 0:
                taked[i-1] = True
            else:
                taked[-i-1] = False
        if support > best_support and min_angle <= angle <= max_angle:
            best_support = support
            best_angle = angle
        if rgb is not None and angle % 20 == 0:
            img = rgb.copy()
            for i, p in enumerate(points):
                if taked[i]:
                    img[p[1]][p[0]] = [0, 255, 0]
            contour = ph.rotate_contour(rect_contour, tuple(center), angle)
            cv2.drawContours(img, [contour], -1, (255, 0, 0), 2)
            sh.show_rgb(img, str(support))
    return best_angle


class Solver:

    def __init__(self):
        self.prev_head = [0, 0, 0]
        self.sieve_mask = None
        pass

    def update_sieve_mask(self, shape):
        print('Updating sieve mask')
        self.sieve_mask = np.zeros(shape=shape, dtype=np.bool)
        for y in range(0, shape[0], 2):
            for x in range(0, shape[1], 2):
                self.sieve_mask[y, x] = True

    def process(self, data, verbose=False, visualize=False):
        depth, mask, rgb = data
        mask = mask.astype(np.bool)

        # head
        head = detect_by_haar(rgb, mask)
        if head is None:
            head = self.prev_head
        self.prev_head = head

        if self.sieve_mask is None or self.sieve_mask.shape != mask.shape:
            self.update_sieve_mask(mask.shape)
        mask &= self.sieve_mask

        head_x, head_y = head
        head_x = int(head_x)
        head_y = int(head_y)
        head = [head_x, head_y, depth[head_y, head_x]]
        head_w, head_h = 30, 50
        head_contour = np.array([(head_x - head_w // 2, head_y - head_h // 2),
                                 (head_x - head_w // 2, head_y + head_h // 2),
                                 (head_x + head_w // 2, head_y + head_h // 2),
                                 (head_x + head_w // 2, head_y - head_h // 2)])
        if visualize:
            cv2.circle(rgb, (int(head_x), int(head_y)), 5, (255, 0, 0), 2)

        ys, xs = np.nonzero(mask)
        points = np.transpose([xs, ys])
        head_angle = find_rect_rotation_center(head_contour, -45, 45, points)
        if verbose:
            print('Head Angle: ', head_angle)
        head_contour = ph.rotate_contour(head_contour, (head_x, head_y), head_angle)
        if visualize:
            cv2.drawContours(rgb, [head_contour], -1, (255, 0, 0), 2)

        neck = np.mean([head_contour[1], head_contour[2]], axis=0)
        neck_x, neck_y = neck[0], neck[1]
        body_w, body_h = 75, 150
        body_x = int(neck_x)
        body_y = int(neck_y + body_h / 2)
        body_contour = np.array([(body_x - body_w // 2, body_y - body_h // 2),
                                 (body_x - body_w // 2, body_y + body_h // 2),
                                 (body_x + body_w // 2, body_y + body_h // 2),
                                 (body_x + body_w // 2, body_y - body_h // 2)])
        if visualize:
            cv2.circle(rgb, (int(neck_x), int(neck_y)), 5, (0, 0, 0), 2)

        points = np.transpose([xs, ys])
        body_angle = find_rect_rotaion_top(body_contour, -45, 45, points)
        if verbose:
            print('Body angle: ', body_angle)
        body_contour = ph.rotate_contour(body_contour, (neck_x, neck_y), body_angle)
        if visualize:
            cv2.drawContours(rgb, [body_contour], -1, (0, 255, 0), 2)

        forearm_w, forearm_h = 15, 50

        right_forearm = tuple(body_contour[0] + (body_contour[1] - body_contour[0]) * 0.1)
        right_forearm_x, right_forearm_y = int(right_forearm[0]), int(right_forearm[1])
        right_forearm_contour = np.array([(right_forearm_x - forearm_w // 2, right_forearm_y),
                                          (right_forearm_x - forearm_w // 2, right_forearm_y + forearm_h),
                                          (right_forearm_x + forearm_w // 2, right_forearm_y + forearm_h),
                                          (right_forearm_x + forearm_w // 2, right_forearm_y)])
        if visualize:
            cv2.circle(rgb, (right_forearm_x, right_forearm_y), 5, (0, 0, 0), 2)
        points = np.array(
            [[xs[i], ys[i]] for i in range(len(xs)) if not ph.is_inside_rectangle(body_contour, [xs[i], ys[i]])])
        right_forearm_angle = find_rect_rotaion_top(right_forearm_contour, -180, 0, points)
        right_forearm_contour = ph.rotate_contour(right_forearm_contour, right_forearm, right_forearm_angle)
        if verbose:
            print('Right Forearm angle: ', right_forearm_angle)
        if visualize:
            cv2.drawContours(rgb, [right_forearm_contour], -1, (255, 255, 0), 2)

        left_forearm = tuple(body_contour[3] + (body_contour[2] - body_contour[3]) * 0.1)
        left_forearm_x, left_forearm_y = int(left_forearm[0]), int(left_forearm[1])
        left_forearm_contour = np.array([(left_forearm_x - forearm_w // 2, left_forearm_y),
                                         (left_forearm_x - forearm_w // 2, left_forearm_y + forearm_h),
                                         (left_forearm_x + forearm_w // 2, left_forearm_y + forearm_h),
                                         (left_forearm_x + forearm_w // 2, left_forearm_y)])
        if visualize:
            cv2.circle(rgb, (left_forearm_x, left_forearm_y), 5, (0, 0, 0), 2)
        points = np.array(
            [[xs[i], ys[i]] for i in range(len(xs)) if not ph.is_inside_rectangle(body_contour, [xs[i], ys[i]])])
        left_forearm_angle = find_rect_rotaion_top(left_forearm_contour, 0, 180, points)
        left_forearm_contour = ph.rotate_contour(left_forearm_contour, left_forearm, left_forearm_angle)
        if verbose:
            print('Left Forearm angle: ', left_forearm_angle)
        if visualize:
            cv2.drawContours(rgb, [left_forearm_contour], -1, (255, 255, 0), 2)

        hand_w, hand_h = 15, 75

        right_hand = tuple(np.mean([right_forearm_contour[1], right_forearm_contour[2]], axis=0))
        right_hand_x, right_hand_y = int(right_hand[0]), int(right_hand[1])
        right_hand_contour = np.array([(right_hand_x - hand_w // 2, right_hand_y),
                                       (right_hand_x - hand_w // 2, right_hand_y + hand_h),
                                       (right_hand_x + hand_w // 2, right_hand_y + hand_h),
                                       (right_hand_x + hand_w // 2, right_hand_y)])
        if visualize:
            cv2.circle(rgb, (right_hand_x, right_hand_y), 5, (0, 0, 0), 2)
        points = np.array(
            [[xs[i], ys[i]] for i in range(len(xs))
             if not ph.is_inside_rectangle(body_contour, [xs[i], ys[i]])
             and not ph.is_inside_rectangle(right_forearm_contour, [xs[i], ys[i]])])
        right_hand_angle = find_rect_rotaion_top(right_hand_contour, -180, 180, points)
        right_hand_contour = ph.rotate_contour(right_hand_contour, right_hand, right_hand_angle)
        if verbose:
            print('Right Hand angle: ', right_hand_angle)
        if visualize:
            cv2.drawContours(rgb, [right_hand_contour], -1, (0, 255, 255), 2)
        wristR_xy = np.mean(right_hand_contour[1:3], axis=0, dtype=np.int)
        x, y = wristR_xy[0], wristR_xy[1]
        wristR = [x, y, depth[y, x]]

        left_hand = tuple(np.mean([left_forearm_contour[1], left_forearm_contour[2]], axis=0))
        left_hand_x, left_hand_y = int(left_hand[0]), int(left_hand[1])
        left_hand_contour = np.array([(left_hand_x - hand_w // 2, left_hand_y),
                                      (left_hand_x - hand_w // 2, left_hand_y + hand_h),
                                      (left_hand_x + hand_w // 2, left_hand_y + hand_h),
                                      (left_hand_x + hand_w // 2, left_hand_y)])
        if visualize:
            cv2.circle(rgb, (left_hand_x, left_hand_y), 5, (0, 0, 0), 2)
        points = np.array(
            [[xs[i], ys[i]] for i in range(len(xs))
             if not ph.is_inside_rectangle(body_contour, [xs[i], ys[i]])
             and not ph.is_inside_rectangle(left_forearm_contour, [xs[i], ys[i]])])
        left_hand_angle = find_rect_rotaion_top(left_hand_contour, -180, 180, points)
        left_hand_contour = ph.rotate_contour(left_hand_contour, left_hand, left_hand_angle)
        if verbose:
            print('Left Hand angle: ', left_hand_angle)
        if visualize:
            cv2.drawContours(rgb, [left_hand_contour], -1, (0, 255, 255), 2)
        wristL_xy = np.mean(left_hand_contour[1:3], axis=0, dtype=np.int)
        x, y = wristL_xy[0], wristL_xy[1]
        wristL = [x, y, depth[y, x]]

        head = ph.back_project(head)
        head = np.add(head, [0, 0, 100])
        wristL = ph.back_project(wristL)
        wristR = ph.back_project(wristR)
        ans = np.array([head, wristL, wristR], dtype=np.float32)
        return ans


def main():
    dr = DataReader('data/test2')
    # while dr.move_next():
    solver = Solver()
    dr.move_to("0000120.png")
    sieve_mask = None
    for _ in range(2):
        # dr.move_next()
        print(dr.get_filename())
        depth, mask, rgb = dr.get_data()
        ans = solver.process((depth, mask, rgb))
        true_ans = dr.get_ans()
        sh.show_ans(rgb, ans, true_ans)
        mh.calc_score(true_ans, ans, verbose=True)


if __name__ == '__main__':
    main()
