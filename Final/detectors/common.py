import numpy as np
from random import randint


def random_point(data):
    depth, mask, _ = data
    ys, xs = np.nonzero(mask)
    index = randint(0, len(ys)-1)
    x, y = xs[index], ys[index]
    return [x, y, depth[y][x]]