import cv2
import numpy as np
import constants
import utils.points_helpers as ph
import utils.show_helpers as sh
import os


def detect_by_haar(data):
    depth, mask, rgb = data
    ys, xs = np.nonzero(mask)
    min_y = ys[0]
    max_y = ys[-1]
    thresh_y = min_y + (max_y - min_y) * constants.HEAD_HEIGHT_RATIO

    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    gray[~mask] = 0
    cascades_dir = 'cascades'
    for file in os.listdir(cascades_dir):
        cascade = cv2.CascadeClassifier(os.path.join(cascades_dir, file))
        faces = cascade.detectMultiScale(gray, 1.05, 5)
        max_area = 0
        x, y, w, h = 0, 0, 0, 0
        if len(faces) > 1:
            print("FACES", len(faces))
        for _x, _y, _w, _h in faces:
            if _w * _h > max_area and mask[_y + _h // 2, _x + _w // 2] and _y + _h // 2 < thresh_y:
                max_area = _w * _h
                x, y, w, h = _x, _y, _w, _h
        if max_area > 0:
            xh = x + w // 2
            yh = y + w // 2
            return [xh, yh, depth[yh, xh]]
    return None


def detect_by_top_point(data):
    depth, mask, rgb = data
    for y, row in enumerate(mask):
        xs = np.nonzero(row)[0]
        if len(xs) > 0:
            x = np.median(xs)
            head = [x, y]
            break
    hx, hy = ph.find_closest_nonzero(head, depth)
    head = [hx, hy, depth[hy, hx]]
    head = ph.move_by_real_dist(head, [0, -constants.HEAD_HEIGHT / 2, 0])
    return head


def detect_top_pca(data):
    depth, mask, rgb = data
    ys, xs = np.nonzero(mask)
    points = np.transpose([xs, ys]).astype(np.uint16)
    m, ev = cv2.PCACompute(points, None, cv2.PCA_DATA_AS_COL)
    mx, my = int(m[0][0]), int(m[0][1])
    q = [(mx, my)]
    xs = np.nonzero(mask[my])[0]
    lx = xs[0]
    rx = xs[-1]
    delta = int((rx - lx) * 0.2)
    lx += delta
    rx -= delta
    h = 0
    used = np.zeros_like(mask, dtype=np.bool)
    used[my, mx] = True
    head = (mx, my)
    while h < len(q):
        cx, cy = q[h]
        h += 1
        if cy < head[1]:
            head = (cx, cy)
        for x in range(cx - 1, cx + 2):
            if lx <= x <= rx:
                for y in range(cy - 1, cy + 2):
                    if y <= cy and mask[cy, cx] and not used[y, x]:
                        used[y, x] = True
                        q.append((x, y))

    temp = rgb.copy()
    temp[~used] = [255, 255, 255]
    sh.show_rgb(temp)

    hx, hy = ph.find_closest_nonzero(head, depth)
    head = [hx, hy, depth[hy, hx]]
    head = ph.move_by_real_dist(head, [0, -constants.HEAD_HEIGHT / 2, 0])
    return head
    # print('m', m)
    # print('ev', ev)
    # p1 = m[0]
    # p2 = m[0] - ev[0] * 200
    # img = rgb.copy()
    # print('p1', p1)
    # print('p2', p2)
    # cv2.line(img, ph.int_tuple2(p1), ph.int_tuple2(p2), (255, 0, 0), thickness=2)
    # sh.show_rgb(img)
