import cv2
import numpy as np
import utils.points_helpers as ph
import utils.show_helpers as sh
from detectors.head import detect_by_haar
from math import hypot, sqrt, acos
import math
import constants
import detectors.common as cd


def detect_by_fitting(solver, head):
    depth, mask, rgb = solver.depth, solver.mask, solver.rgb
    # head = ph.project(head)

    img = rgb.copy()

    avg_z = np.average(depth, weights=mask)

    hx, hy, hz = head
    hw = constants.HEAD_WIDTH
    hh = constants.HEAD_HEIGHT
    head_contour = np.array([ph.move_by_real_dist(head, [-hw / 2, -hh / 2, 0]),
                             ph.move_by_real_dist(head, [-hw / 2, +hh / 2, 0]),
                             ph.move_by_real_dist(head, [+hw / 2, +hh / 2, 0]),
                             ph.move_by_real_dist(head, [+hw / 2, -hh / 2, 0])])
    head_contour = head_contour[:, :2]

    ys, xs = np.nonzero(mask & solver.sieve_mask)
    points = np.transpose([xs, ys])
    head_angle = 0  # find_rect_rotation_center(head_contour, -45, 45, points)
    # print('Head Angle: ', head_angle)
    head_contour = ph.rotate_contour(head_contour, ph.int_tuple2(head), head_angle)
    cv2.drawContours(img, [head_contour], -1, (255, 0, 0), 2)

    neck = np.mean([head_contour[0], head_contour[3]], axis=0)
    bw = constants.BODY_WIDTH
    bh = constants.BODY_HEIGHT
    neck = np.concatenate((neck, [avg_z]))
    cv2.circle(img, ph.int_tuple2(neck), 5, (0, 0, 0))
    body_contour = np.array([ph.move_by_real_dist(neck, [-bw / 2, 0, 0]),
                             ph.move_by_real_dist(neck, [-bw / 2, -bh / 2, 0]),
                             ph.move_by_real_dist(neck, [+bw / 2, -bh / 2, 0]),
                             ph.move_by_real_dist(neck, [+bw / 2, 0, 0])])
    body_contour = body_contour[:, :2]

    body_angle = find_rect_rotaion_top(body_contour, -45, 45, points, avg_z=solver.med_z, depth=depth)
    # print('Body angle: ', body_angle)
    body_contour = ph.rotate_contour(body_contour, ph.int_tuple2(neck), body_angle)
    cv2.drawContours(img, [body_contour], -1, (0, 255, 0), 2)

    fw = constants.FOREARM_WIDTH
    fh = constants.FOREARM_HEIGHT

    right_forearm = np.average([body_contour[0], body_contour[1]], weights=[5, 1], axis=0)
    rf_joint = np.concatenate((right_forearm, [avg_z]))
    right_forearm_contour = np.array([ph.move_by_real_dist(rf_joint, [-fw / 2, 0, 0]),
                                      ph.move_by_real_dist(rf_joint, [-fw / 2, -fh, 0]),
                                      ph.move_by_real_dist(rf_joint, [+fw / 2, -fh, 0]),
                                      ph.move_by_real_dist(rf_joint, [+fw / 2, 0, 0])])
    right_forearm_contour = right_forearm_contour[:, :2]
    cv2.circle(img, ph.int_tuple2(right_forearm), 5, (0, 0, 0), 2)
    # print('RF', right_forearm_contour)
    #points = [p for p in points if not ph.is_inside_rectangle(body_contour, p)]
    right_forearm_angle = find_rect_rotaion_top(right_forearm_contour, -180, 0, points)
    right_forearm_contour = ph.rotate_contour(right_forearm_contour, right_forearm, right_forearm_angle)
    # print('Right Forearm angle: ', right_forearm_angle)
    cv2.drawContours(img, [right_forearm_contour], -1, (255, 255, 0), 2)

    left_forearm = np.average([body_contour[3], body_contour[2]], weights=[5, 1], axis=0)
    lf_joint = np.concatenate((left_forearm, [avg_z]))
    left_forearm_contour = np.array([ph.move_by_real_dist(lf_joint, [-fw / 2, 0, 0]),
                                     ph.move_by_real_dist(lf_joint, [-fw / 2, -fh, 0]),
                                     ph.move_by_real_dist(lf_joint, [+fw / 2, -fh, 0]),
                                     ph.move_by_real_dist(lf_joint, [+fw / 2, 0, 0])])
    left_forearm_contour = left_forearm_contour[:, :2]
    cv2.circle(img, ph.int_tuple2(left_forearm), 5, (0, 0, 0), 2)
    # print('LF', left_forearm_contour)
    left_forearm_angle = find_rect_rotaion_top(left_forearm_contour, 0, 180, points)
    left_forearm_contour = ph.rotate_contour(left_forearm_contour, left_forearm, left_forearm_angle)
    # print('Left Forearm angle: ', left_forearm_angle)
    cv2.drawContours(img, [left_forearm_contour], -1, (255, 255, 0), 2)

    hand_w = constants.HAND_WIDTH
    hand_h = constants.HAND_HEIGHT

    right_hand = np.mean(right_forearm_contour[1:3], axis=0)
    rh_joint = np.concatenate((right_hand, [avg_z]))
    right_hand_contour = np.array([ph.move_by_real_dist(rh_joint, [-hand_w / 2, 0, 0]),
                                   ph.move_by_real_dist(rh_joint, [-hand_w / 2, -hand_h, 0]),
                                   ph.move_by_real_dist(rh_joint, [+hand_w / 2, -hand_h, 0]),
                                   ph.move_by_real_dist(rh_joint, [+hand_w / 2, 0, 0])])[:, :2]
    cv2.circle(img, ph.int_tuple2(right_hand), 5, (0, 0, 0), 2)
    right_hand_angle = find_rect_rotaion_top(right_hand_contour, -180, 180, points)
    right_hand_contour = ph.rotate_contour(right_hand_contour, right_hand, right_hand_angle)
    # print('Right Hand angle: ', right_hand_angle)
    cv2.drawContours(img, [right_hand_contour], -1, (0, 255, 255), 2)
    wristR_xy = np.mean(right_hand_contour[1:3], axis=0, dtype=np.int)
    x, y = ph.find_closest_nonzero(wristR_xy, depth)
    wrist_r = [x, y, depth[y, x]]

    left_hand = np.mean(left_forearm_contour[1:3], axis=0)
    lh_joint = np.concatenate((left_hand, [avg_z]))
    left_hand_contour = np.array([ph.move_by_real_dist(lh_joint, [-hand_w / 2, 0, 0]),
                                   ph.move_by_real_dist(lh_joint, [-hand_w / 2, -hand_h, 0]),
                                   ph.move_by_real_dist(lh_joint, [+hand_w / 2, -hand_h, 0]),
                                   ph.move_by_real_dist(lh_joint, [+hand_w / 2, 0, 0])])[:, :2]
    cv2.circle(img, ph.int_tuple2(left_hand), 5, (0, 0, 0), 2)
    left_hand_angle = find_rect_rotaion_top(left_hand_contour, -180, 180, points)
    left_hand_contour = ph.rotate_contour(left_hand_contour, left_hand, left_hand_angle)
    # print('left Hand angle: ', left_hand_angle)
    cv2.drawContours(img, [left_hand_contour], -1, (0, 255, 255), 2)
    wristL_xy = np.mean(left_hand_contour[1:3], axis=0, dtype=np.int)
    x, y = ph.find_closest_nonzero(wristL_xy, depth)
    wrist_l = [x, y, depth[y, x]]

    sh.show_rgb(img)
    return wrist_l, wrist_r


def find_rect_rotation_center(rect_contour, min_angle, max_angle, points):
    bins = [0 for _ in range(180)]
    center = np.mean([rect_contour[0], rect_contour[2]], axis=0)
    w = np.linalg.norm(rect_contour[2] - rect_contour[1])
    h = np.linalg.norm(rect_contour[1] - rect_contour[0])
    for p in points:
        dist = np.linalg.norm(p - center)
        if dist <= w / 2:
            pass
        elif dist <= h / 2:
            a = sqrt(dist ** 2 - (w / 2) ** 2)
            q = np.mean([rect_contour[2], rect_contour[3]], axis=0) + (rect_contour[2] - rect_contour[3]) * a / h
            u = q - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            alpha = round(alpha)
            if alpha < -90:
                alpha += 180
            if alpha >= 90:
                alpha -= 180
            delta = acos((2 * dist ** 2 - w ** 2) / (2 * dist ** 2)) * 180 / math.pi
            beta = round(alpha + delta)
            if beta < -90:
                beta += 180
            if beta >= 90:
                beta -= 180
            if alpha <= beta:
                bins[alpha + 90] += 1
                bins[beta + 90] -= 1
            else:
                bins[alpha + 90] += 1
                bins[179] -= 1
                bins[0] += 1
                bins[beta + 90] -= 1
    best_support = 0
    best_angle = 0
    support = 0
    for angle in range(-90, 90):
        support += bins[angle + 90]
        if support > best_support and min_angle <= angle <= max_angle:
            best_support = support
            best_angle = angle
    return best_angle


def find_rect_rotaion_top(rect_contour, min_angle, max_angle, points, rgb=None, avg_z=None, depth=None):
    bins = [0 for _ in range(360)]
    bin_points = [[] for _ in range(360)]
    taked = np.zeros(shape=(len(points)), dtype=np.bool)
    center = np.mean([rect_contour[0], rect_contour[3]], axis=0)
    w = np.linalg.norm(rect_contour[2] - rect_contour[1])
    h = np.linalg.norm(rect_contour[1] - rect_contour[0])

    def norm(a):
        if a < -180:
            a += 360
        if a >= 180:
            a -= 360
        return a

    def add_segment(alpha, beta, i):
        alpha = norm(round(alpha))
        beta = norm(round(beta))
        if alpha <= beta:
            bins[alpha + 180] += 1
            bin_points[alpha + 180].append(i)
            bins[beta + 180] -= 1
            bin_points[beta + 180].append(-i)
        else:
            bins[alpha + 180] += 1
            bin_points[alpha + 180].append(i)
            bins[359] -= 1
            bin_points[359].append(-i)
            bins[0] += 1
            bin_points[0].append(i)
            bins[beta + 180] -= 1
            bin_points[beta + 180].append(-i)

    r = hypot(h, w / 2)
    for i, p in enumerate(points):
        if p[0] == -1:
            continue
        if avg_z is not None and abs(depth[p[1], p[0]] - avg_z) > constants.Z_DIST_THRESHOLD:
            continue
        dist = np.linalg.norm(p - center)
        if dist < 1e-6:
            pass
        elif dist <= w / 2:
            u = rect_contour[3] - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            beta = alpha + 180
            add_segment(alpha, beta, i + 1)
        elif dist <= h:
            a = sqrt(dist ** 2 - (w / 2) ** 2)
            q = rect_contour[3] + (rect_contour[2] - rect_contour[3]) * a / h
            u = q - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            delta = acos((2 * dist ** 2 - w ** 2) / (2 * dist ** 2)) * 180 / math.pi
            beta = alpha + delta
            add_segment(alpha, beta, i + 1)
        elif dist < r:
            a = sqrt(dist ** 2 - (w / 2) ** 2)
            q = rect_contour[3] + (rect_contour[2] - rect_contour[3]) * a / h
            u = q - center
            v = p - center
            alpha = angle_between_vectors(u, v)
            b = w / 2 - sqrt(dist ** 2 - h ** 2)
            c = hypot(h - a, b)
            delta1 = acos((2 * dist ** 2 - c ** 2) / (2 * dist ** 2)) * 180 / math.pi
            delta = acos((2 * dist ** 2 - w ** 2) / (2 * dist ** 2)) * 180 / math.pi
            beta = alpha + delta
            add_segment(alpha, alpha + delta1, i + 1)
            add_segment(beta - delta1, beta, i + 1)

    best_support = -1
    best_angle = 0
    best_points = None
    support = 0
    for angle in range(-180, 180):
        support += bins[angle + 180]
        for i in bin_points[angle + 180]:
            if i > 0:
                taked[i - 1] = True
            else:
                taked[-i - 1] = False
        if support > best_support and min_angle <= angle <= max_angle:
            best_support = support
            best_angle = angle
            best_points = taked.copy()
        if rgb is not None and angle % 20 == 0:
            img = rgb.copy()
            for i, p in enumerate(points):
                if taked[i]:
                    img[p[1]][p[0]] = [0, 255, 0]
            contour = ph.rotate_contour(rect_contour, tuple(center), angle)
            cv2.drawContours(img, [contour], -1, (255, 0, 0), 2)
            sh.show_rgb(img, str(support))
            cv2.imwrite('D:/temp/' + '{0:02d}'.format(int((angle + 180)/20)) + '.png', cv2.cvtColor(img, cv2.COLOR_RGB2BGR))
    points[best_points] = [-1, -1]
    return best_angle


def angle_between_vectors(u, v):
    cs = np.dot(u, v) / np.linalg.norm(u) / np.linalg.norm(v)
    cs = max(cs, -1.0 + 1e-6)
    cs = min(cs, 1.0 - 1e-6)
    sn = u[0] * v[1] - u[1] * v[0]
    angle = -acos(cs) * 180 / math.pi
    if sn < 0:
        angle = -angle
    return angle
