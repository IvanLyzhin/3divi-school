import utils.show_helpers as sh
from utils.data_reader import DataReader
import utils.points_helpers as ph

import numpy as np
import cv2


class Solver:

    def __init__(self):
        pass

    def process(self, data):
        depth, mask, rgb = data
        mask = mask.astype(np.bool)



        return None


def main():
    dr = DataReader('data/test1')
    # while dr.move_next():
    solver = Solver()
    sieve_mask = None
    db_des = []
    for _ in range(50):
        dr.move_next()
        print(dr.get_filename())
        depth, mask, rgb = dr.get_data()
        true_ans = dr.get_ans()

        def crop_hand(hand_pos):
            wrist = ph.project(hand_pos)
            lx, ly, _ = wrist
            lx = int(lx)
            ly = int(ly)
            r = 30
            gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
            cropped = gray[ly-r:ly+r, lx-r:lx+r]

            sift = cv2.xfeatures2d.SIFT_create(nfeatures=10)
            kp, des = sift.detectAndCompute(cropped, None)
            kp_cleared = []
            mask_cropped = mask[ly-r:ly+r, lx-r:lx+r]
            for i, p in enumerate(kp):
                x, y = p.pt
                x = int(x)
                y = int(y)
                if mask_cropped[y, x]:
                    kp_cleared.append(p)
                    db_des.append(des[i])

        crop_hand(true_ans[1])
        crop_hand(true_ans[2])

    db_des = np.array(db_des)
    print("DB", db_des.shape[0])

    for _ in range(10):
        dr.move_next()
        _, mask, rgb = dr.get_data()
        sift = cv2.xfeatures2d.SIFT_create(nfeatures=500)
        gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
        kp, des = sift.detectAndCompute(gray, None)
        kp2 = []
        des2 = []
        for i, p in enumerate(kp):
            x, y = p.pt
            x = int(x)
            y = int(y)
            if mask[y][x]:
                kp2.append(p)
                des2.append(des[i])
        des = np.array(des2)
        kp = kp2
        print(len(kp))
        temp = cv2.drawKeypoints(rgb, kp, None, color=(255, 0, 0))
        bf = cv2.BFMatcher()
        print(des.shape, db_des.shape)
        matches = bf.match(des, db_des)
        print(len(matches))
        matches = sorted(matches, key=lambda x: x.distance)
        best_kps = []
        for m in matches[:50]:
            best_kps.append(kp[m.queryIdx])
        temp = cv2.drawKeypoints(temp, best_kps, None, color=(0, 255, 0))
        sh.show_rgb(temp)



if __name__ == '__main__':
    main()
