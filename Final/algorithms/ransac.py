import cv2
import numpy as np
import utils.points_helpers as ph
import math
import utils.show_helpers as sh


def find_rect_rotation(rect_contour, rot_center, min_angle, max_angle, points, iters=10):
    best_angle = 0
    best_support = 0
    for cur_angle in np.linspace(min_angle, max_angle, iters):
        # print('Cur angle: ', cur_angle)
        contour = ph.rotate_contour(rect_contour, rot_center, cur_angle)
        support = 0
        for p in points:
            if ph.is_inside_rectangle(contour, p):
                support += 1
        # print('Support: ', support)
        if support > best_support:
            best_support = support
            best_angle = cur_angle
    return best_angle


def __get_angle(point, center):
    return -180 * math.atan2(point[1] - center[1], point[0] - center[0]) / math.pi


def find_rect_rotation_fast(rect_contour, rot_center, min_angle, max_angle, points, verbose=False, rgb=None):
    best_angle = 0
    best_support = -1000000000
    support = 0
    points = sorted(points, key=lambda p: __get_angle(p, rot_center))
    c_x, c_y = rot_center
    p_angles = [__get_angle(p, rot_center) for p in points]
    l, r = 0, 0
    taked = [False]*len(points)
    delta = 15
    if verbose:
        print(rot_center)
    for angle in np.linspace(-180, 180, 90):
        # p_angle = __get_angle(pi, rot_center)
        contour = ph.rotate_contour(rect_contour, rot_center, angle)
        while r < len(points) and p_angles[r] < angle + delta:
            if ph.is_inside_rectangle(contour, points[r]):
                support += 1
                taked[r] = True
            r += 1
        while l < r and p_angles[l] < angle - delta:
            if taked[l]:
                support -= 1
            l += 1
        if verbose:
            print(angle, support)
            if rgb is not None:
                img = rgb.copy()
                cv2.drawContours(img, [contour], -1, (255, 255, 255), 2)
                for i in range(l, r):
                    if taked[i]:
                        img[points[i][1], points[i][0]] = [0, 255, 0]
                sh.show_rgb(img, str(support))
        if min_angle <= angle <= max_angle and support > best_support:
            best_support = support
            best_angle = angle
    return best_angle
