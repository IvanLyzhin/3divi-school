import numpy as np
from sklearn.cluster import KMeans, DBSCAN
from utils.random_colors import get_colors
import queue
import utils.points_helpers as ph
import random


def build_mosaic_kmeans(mask, n_clusters):
    mosaic = -np.ones(mask.shape, dtype=np.int8)
    ys, xs = np.nonzero(mask)
    points = np.transpose([xs, ys])
    kmeans = KMeans(n_clusters=n_clusters, random_state=0, n_init=3).fit(points)
    labels = kmeans.labels_
    for i, p in enumerate(points):
        x, y = p[0], p[1]
        mosaic[y][x] = labels[i]
    return mosaic, kmeans.cluster_centers_


def build_mosaic_dbscan(mask):
    mosaic = -np.ones(mask.shape, dtype=np.int8)
    min_y = np.min(np.nonzero(mask)[0])
    max_y = np.max(np.nonzero(mask)[0])
    knee_y = int(max_y - (max_y - min_y) * 0.2)
    print("KNEE_Y: ", knee_y)
    points = [[x, y] for y in range(knee_y) for x in range(mask.shape[1]) if mask[y, x]]
    dbscan = DBSCAN(eps=10, min_samples=300).fit(points)
    labels = dbscan.labels_
    for i, p in enumerate(points):
        x, y = p[0], p[1]
        mosaic[y][x] = labels[i]
    return mosaic, dbscan.components_


def build_mosaic_bfs(mask, cluster_radius):
    height, width = mask.shape
    mosaic = -np.ones(shape=mask.shape, dtype=np.int16)
    n_clusters = 0
    centers = []
    points = np.transpose(np.nonzero(mask))
    random.shuffle(points)
    for y in range(mask.shape[0]):
        for x in range(mask.shape[1]):
            if mask[y, x] and mosaic[y, x] == -1:
                mosaic[y, x] = n_clusters
                cluster_points = []
                q = queue.Queue()
                q.put([x, y], block=False)
                cluster_points.append([x, y])
                while not q.empty():
                    p = q.get_nowait()
                    for x2, y2 in ph.get_neighbours(p, mask.shape):
                        if mask[y2, x2] and mosaic[y2, x2] == -1 \
                                and np.linalg.norm(np.subtract([x, y], [x2, y2])) < cluster_radius:
                            mosaic[y2, x2] = n_clusters
                            cluster_points.append([x2, y2])
                            q.put([x2, y2])
                n_clusters += 1
                centers.append(np.mean(cluster_points, axis=0))
    return mosaic, centers


def build_mosaic_img(mosaic):
    height, width = mosaic.shape
    mosaic_img = np.zeros(shape=(height, width, 3), dtype=np.uint8)
    colors = get_colors(np.max(mosaic)+1)
    for y in range(height):
        for x in range(width):
            if mosaic[y, x] != -1:
                mosaic_img[y, x] = colors[mosaic[y, x]]
    return mosaic_img
