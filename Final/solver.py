import numpy as np
from detectors import head as hd
from detectors import common as cd
from detectors import wrists as wd
import utils.show_helpers as sh
import utils.points_helpers as ph
from utils.data_reader import DataReader
from time import time
import constants


class Solver:
    def __init__(self):
        self.data = None
        self.depth = None
        self.mask = None
        self.rgb = None
        self.shape = None
        self.height = None
        self.width = None
        self.med_z = None
        self.sieve_mask = None
        self.prev_head = [0, 0, 0]
        self.prev_wrist_l = [0, 0, 0]
        self.prev_wrist_r = [0, 0, 0]
        pass

    def process(self, data, verbose=False, visualize=False):
        # start = time()
        self.depth, self.mask, self.rgb = data
        self.mask = self.mask.astype(np.bool)
        self.shape = self.mask.shape
        self.height = self.shape[0]
        self.width = self.shape[1]

        self.preprocess(data)

        try:
            head = self.detect_head((self.depth, self.mask, self.rgb))
            self.prev_head = head
        except:
            head = self.prev_head

        try:
            wrist_l, wrist_r = self.detect_wrists(head)
            self.prev_wrist_l = wrist_l
            self.prev_wrist_r = wrist_r
        except:
            wrist_l = self.prev_wrist_l
            wrist_r = self.prev_wrist_r

        head = ph.back_project(ph.move_by_real_dist(head, [0, 0, constants.HEAD_DEPTH]))

        # end = time()
        # print('TIME: {:.2f}'.format(end-start))
        return np.array([head, wrist_l, wrist_r], dtype=np.float32)

    def detect_head(self, data):
        depth, mask, rgb = data
        head = hd.detect_by_haar(data)

        if head is None:
            head = hd.detect_top_pca(data)
        else:
            print('OpenCV found face')
        return head

    def detect_wrists(self, head):
        wrist_l, wrist_r = wd.detect_by_fitting(self, head)
        return ph.back_project(wrist_l), ph.back_project(wrist_r)

    def preprocess(self, data):
        #ys = np.nonzero(self.mask)[0]
        #top_y = ys[0]
        #bottom_y = ys[-1]
        #thresh_y = int(bottom_y - (bottom_y - top_y) * constants.BOTTOM_CUT_RATIO)
        #bot_cut_mask = np.vstack((np.ones((thresh_y, self.width)), np.zeros((self.height - thresh_y, self.width)))).astype(np.bool)
        #self.mask &= bot_cut_mask

        self.rgb[~self.mask] = [0, 0, 0]
        self.depth[~self.mask] = 0

        if self.sieve_mask is None or self.sieve_mask.shape != self.shape:
            self.update_sieve_mask()

        self.med_z = np.median(self.depth[self.depth > 0])

    def update_sieve_mask(self):
        self.sieve_mask = np.zeros(self.shape, dtype=np.bool)
        for y in range(self.shape[0]):
            for x in range(y % constants.SIEVE_STEP, self.shape[1], constants.SIEVE_STEP):
                self.sieve_mask[y, x] = True


if __name__ == "__main__":
    sh.enable_visualization()
    dr = DataReader('data/test4')
    # while dr.move_next():
    solver = Solver()
    #dr.move_next()
    #dr.move_to("0000198.png")
    for _ in range(100):
        dr.move_next()
    for _ in range(1):
        dr.move_next()
        print(dr.get_filename())
        data = dr.get_data()
        ans = solver.process(data)
        print(ans)
        sh.show_ans(data[2], ans)
        #true_ans = dr.get_ans()
        #sh.show_ans(rgb, ans, true_ans)
        #mh.calc_score(true_ans, ans, verbose=True)